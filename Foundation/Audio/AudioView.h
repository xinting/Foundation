//
//  AudioView.h
//  Foundation
//
//  Created by 新庭 on 2020/8/1.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioView : UIView

@property (nonatomic, strong) NSArray<NSArray<NSNumber *> *> *spectra;

@end

NS_ASSUME_NONNULL_END
