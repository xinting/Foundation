//
//  AudioView.m
//  Foundation
//
//  Created by 新庭 on 2020/8/1.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "AudioView.h"

@interface AudioView ()

@property (nonatomic, strong) CAGradientLayer *leftLayer;
@property (nonatomic, strong) CAGradientLayer *rightLayer;

@end

static CGFloat barWidth = 20;
static CGFloat barSpace = 5;
@implementation AudioView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.leftLayer = [CAGradientLayer layer];
        self.rightLayer = [CAGradientLayer layer];
        
        [self.layer addSublayer:self.leftLayer];
        [self.layer addSublayer:self.rightLayer];
    }
    return self;
}

- (void)setSpectra:(NSArray<NSArray<NSNumber *> *> *)spectra {
    _spectra = spectra;
    
    UIBezierPath *leftPath = [UIBezierPath bezierPath];
    for (NSInteger i = 0; i < spectra[0].count; i++) {
        CGFloat height = spectra[0][i].floatValue;
        CGFloat x = barWidth * i + barSpace * i;
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(x, self.bounds.size.height - height, barWidth, height)];
        [leftPath appendPath:path];
    }
    
    self.leftLayer.frame = self.bounds;
}

@end
