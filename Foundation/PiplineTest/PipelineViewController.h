//
//  PipelineViewController.h
//  Foundation
//
//  Created by 三井 on 2020/1/6.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FFoundation/FBaseHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@interface PipelineViewController : PBaseViewController

@end

NS_ASSUME_NONNULL_END
