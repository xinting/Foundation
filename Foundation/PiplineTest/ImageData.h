//
//  ImageData.h
//  Foundation
//
//  Created by 吴新庭 on 2019/6/21.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageData : NSObject

@property (nonatomic, copy) NSString *path;
@property (nonatomic, strong) UIImage *image;

@end

NS_ASSUME_NONNULL_END
