//
//  PipelineViewController.m
//  Foundation
//
//  Created by 三井 on 2020/1/6.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "PipelineViewController.h"
#import <FFoundation/RPipeline.h>
#import <FFoundation/FXCategories.h>
#import <Masonry/Masonry.h>
#import <FFoundation/RPipeline+Filter.h>

@interface PipelineViewController () <RPipelineFilterProtocol>

@property (nonatomic, strong) RPipeline *pipeline;

@property (nonatomic, strong) UITextField *batchSizeField;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UIButton *destoryButton;

@end

@implementation PipelineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareSubviews];
}

- (void)prepareSubviews {
    self.view.backgroundColor = [UIColor whiteColor];
    self.pipeline = [RPipeline pipelineWithBlock:^(RPipelineBatch * _Nonnull batch) {
        NSLog(@"%@", batch.objects);
        sleep(3);
        [batch consumed];
    }];
    self.pipeline.batchSize = 2;
    [self.pipeline addFilter:self];
    
    self.button = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.button setTitle:@"Feed" forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(clickFeed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button];
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_offset(0);
    }];
    
    self.batchSizeField = [[UITextField alloc] init];
    self.batchSizeField.borderStyle = UITextBorderStyleRoundedRect;
    self.batchSizeField.keyboardType = UIKeyboardTypeNumberPad;
    [self.view addSubview:self.batchSizeField];
    [self.batchSizeField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 30));
        make.bottom.mas_equalTo(self.button.mas_top);
        make.centerX.mas_offset(0);
    }];
    
    self.destoryButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.view addSubview:self.destoryButton];
    [self.destoryButton setTitle:@"Destory" forState:UIControlStateNormal];
    [self.destoryButton addTarget:self.pipeline action:@selector(destroy) forControlEvents:UIControlEventTouchUpInside];
    [self.destoryButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_offset(0);
        make.top.mas_equalTo(self.button.mas_bottom).mas_offset(10);
    }];
}

#pragma mark - Action
- (void)clickFeed:(UIButton *)btn {
//    self.pipeline.batchSize = [self.batchSizeField.text integerValue];
    [self.pipeline produce:@1];
}

#pragma mark - Filter
- (NSObject *)pipelineFilter:(NSObject *)object {
    if ([object isKindOfClass:NSNumber.class]) {
        return @(((NSNumber *)object).integerValue + 1);
    }
    return object;
}

@end
