//
//  AlgorithmViewController.m
//  Foundation
//
//  Created by 三井 on 2020/10/12.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "AlgorithmViewController.h"
#import <FFoundation/FRSA.h>
#import <FFoundation/FLinkedItem.h>

@interface AlgorithmViewController ()

@property (nonatomic, strong) FLinkedList<NSNumber *> *linkedList;

@end

@implementation AlgorithmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"生成链表";
        item.action = ^{
            self.linkedList = [[FLinkedList alloc] init];
            for (int i = 0; i < 10; i++) {
                [self.linkedList append:@(arc4random() % 10)];
            }
            FTestLogDebug(@"已生成\n%@", self.linkedList);
        };
    }];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"新增节点100";
        item.action = ^{
            [self.linkedList append:@(100)];
            FTestLogDebug(@"新增后\n%@", self.linkedList);
        };
    }];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"删除位于2的节点";
        item.action = ^{
            [self.linkedList removeAt:2];
            FTestLogDebug(@"删除位于2的节点后\n%@", self.linkedList);
        };
    }];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"删除最后一个";
        item.action = ^{
            [self.linkedList removeAt:9];
            FTestLogDebug(@"删除第9个节点后\n%@", self.linkedList);
        };
    }];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"翻转链表";
        item.action = ^{
            [self.linkedList reverse];
            FTestLogDebug(@"翻转后\n%@", self.linkedList);
        };
    }];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"新增100到第5位前";
        item.action = ^{
            [self.linkedList insert:@100 before:4];
            FTestLogDebug(@"新增后\n%@", self.linkedList);
        };
    }];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"排序";
        item.action = ^{
            [self.linkedList sort:^NSComparisonResult(FLinkedItem * _Nonnull obj1, FLinkedItem * _Nonnull obj2) {
                return [obj1.data compare:obj2.data];
            }];
            FTestLogDebug(@"排序后\n%@", self.linkedList);
        };
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
