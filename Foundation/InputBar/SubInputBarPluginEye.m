//
//  SubInputBarPlugin3.m
//  Foundation
//
//  Created by 新庭 on 2020/2/11.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "SubInputBarPluginEye.h"
#import <FFoundation/FInputBarItem.h>
#import <FFoundation/FXCategories.h>
#import <FFoundation/FInputBar+Plugin.h>

@implementation SubInputBarPluginEye

+ (void)load {
    [[FPluginCenter sharedInstance] registerPlugin:self];
}

+ (NSArray<NSString *> *)hostIdentifiers {
    return @[@"SubInputBar"];
}

+ (NSString *)identifier {
    return @"SubInputBarPluginEye";
}

- (BOOL)plg_load:(FPluginArg *)arg {
    self.arg = arg;
    FInputBarItem *item = [FInputBarItem objectWithBuilder:^(__kindof FInputBarItem * _Nonnull object) {
        object.position = FInputBarItemPositionInnerLeft;
        object.name = @"Plugin";
    }];
    
    arg.inputBar.delegate = self;
    [arg.inputBar plg_beginUpdate];
    [arg.inputBar.plg_datas addObject:item];
    [arg.inputBar plg_endUpdate];
    
    return YES;
}

- (void)plg_refresh:(FPluginArg *)arg {
    
}

- (void)plg_unLoad {
    
}

#pragma mark - Action
- (void)clickFuncButton:(UIButton *)btn {
    FInputBarItem *item = [self.arg.inputBar itemForView:btn];
    item.selected = !item.isSelected;
    
    if (!item.selected) {
        [self.arg.inputBar foldHead];
    } else {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor cyanColor];
        [self.arg.inputBar unfoldHead:view height:40];
    }
}

#pragma mark - InputBar
- (UIView *)inputBar:(FInputBar *)bar willRenderItem:(FInputBarItem *)item {
    if (bar.inputBarTextField.textField.isFirstResponder) {
        return nil;
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setImage:[UIImage imageNamed:@"eye"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickFuncButton:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}
@end
