//
//  InputBarViewController.m
//  Foundation
//
//  Created by 新庭 on 2020/2/5.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "InputBarViewController.h"
#import <FFoundation/FInputBar.h>
#import <Masonry/Masonry.h>
#import <FFoundation/FXCategories.h>
#import "SubInputBar.h"
#import "FInputBar+Plugin.h"

@interface InputBarViewController () <FInputBarDelegate>

@property (nonatomic, strong) FInputBar *inputbar;

@end

@implementation InputBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareSubviews];
}

- (void)prepareSubviews {
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.inputbar];
    
    [self.inputbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_offset(0);
    }];
    self.inputbar.padding = UIEdgeInsetsMake(0, 10, 0, -10);
    self.inputbar.inputBarTextField.padding = UIEdgeInsetsMake(5, 5, 5, 5);
    [self.inputbar safeAreaInsistBarHeight:51];
    
    self.inputbar.pluginPriority = @[@"SubInputBarPlugin1", @"SubInputBarPlugin2", @"SubInputBarPlugin3"];
    [self.inputbar plghost_load:[FPluginArg argWithBuilder:^(FPluginArg *arg) {
        arg.hostIdentifier = @"SubInputBar";
    }]];
    [self.inputbar refresh];
}

#pragma mark - Action

#pragma mark - InputBar
- (UIView *)inputBar:(FInputBar *)bar willRenderItem:(FInputBarItem *)item {
    UIView *view = [bar viewForItem:item];
    if (view) {
        return view;
    }
    
    return view;
}

- (void)inputBar:(FInputBar *)bar foldPanel:(nonnull UIView *)panel {
    
}

- (void)inputBar:(FInputBar *)bar unfoldHead:(UIView *)head {
    NSLog(@"Unfold Head %@", head);
}

- (void)inputBar:(FInputBar *)bar foldHead:(UIView *)head {
    NSLog(@"Fold Head %@", head);
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.inputbar refresh];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.inputbar refresh];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    [self.inputbar foldHead];
    [self.inputbar foldPanel];
    [self.inputbar.items enumerateObjectsUsingBlock:^(FInputBarItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.selected = NO;
    }];
}

- (FInputBar *)inputbar {
    if (!_inputbar) {
        _inputbar = [[SubInputBar alloc] init];
        _inputbar.delegate = self;
        _inputbar.backgroundColor = [UIColor redColor];
    }
    return _inputbar;
}
@end
