//
//  SubInputBarPlugin1.h
//  Foundation
//
//  Created by 新庭 on 2020/2/11.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FFoundation/FPluginHeaders.h>
#import <FFoundation/FInputBar.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubInputBarPlugin1 : NSObject <FPlugin, FInputBarDelegate>

@property (nonatomic, strong) FPluginArg *arg;

@end

NS_ASSUME_NONNULL_END
