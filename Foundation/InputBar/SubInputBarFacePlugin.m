//
//  SubInputBarFacePlugin.m
//  Foundation
//
//  Created by 新庭 on 2020/2/11.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "SubInputBarFacePlugin.h"
#import <FFoundation/FXCategories.h>

@implementation SubInputBarFacePlugin

+ (void)load {
    [[FPluginCenter sharedInstance] registerPlugin:self];
}

+ (NSArray<NSString *> *)hostIdentifiers {
    return @[@"SubInputBar"];
}

+ (NSString *)identifier {
    return @"InputBarFacePlugin";
}

- (BOOL)plg_load:(FPluginArg *)arg {
    self.arg = arg;
    arg.inputBar.delegate = self;
    
    [arg.inputBar plg_beginUpdate];
    [arg.inputBar.plg_datas addObject:[FInputBarItem objectWithBuilder:^(__kindof FInputBarItem * _Nonnull object) {
        ((FInputBarItem *)object).position = FInputBarItemPositionRight;
    }]];
    [arg.inputBar plg_endUpdate];
    
    return YES;
}

- (void)plg_refresh:(FPluginArg *)arg {
    
}

- (void)plg_unLoad {
    
}

#pragma mark - Action
- (void)clickFaceButton:(UIButton *)btn {
    FInputBarItem *item = [self.arg.inputBar itemForView:btn];
    item.selected = !item.selected;
    
    if (!item.selected) {
        [self.arg.inputBar unfoldPanel:nil height:0];
        return;
    }
    UIView *facePanel = [[UIView alloc] init];
    facePanel.backgroundColor = [UIColor yellowColor];
    [self.arg.inputBar unfoldPanel:facePanel height:FInputBarPanelDefaultHeight - 20];
}

#pragma mark - InputBar
- (nonnull UIView *)inputBar:(nonnull FInputBar *)bar willRenderItem:(nonnull FInputBarItem *)item {
    if (!bar.inputBarTextField.textField.isFirstResponder) {
        return nil;
    }
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"F" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickFaceButton:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

@end
