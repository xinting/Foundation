//
//  SubInputBar.m
//  Foundation
//
//  Created by 新庭 on 2020/2/11.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "SubInputBar.h"
#import "FInputBar+Plugin.h"

@implementation SubInputBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (NSMutableArray<FInputBarItem *> *)items {
    return self.plg_datas;
}

- (void)refresh {
    [super refresh];
}

@end
