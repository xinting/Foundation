//
//  AppDelegate.h
//  Foundation
//
//  Created by 吴新庭 on 2019/4/1.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FFoundation/FTestHeaders.h>

@interface AppDelegate : FTestAppDelegate <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

