//
//  PluginTest2.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import "PluginTest2.h"

@implementation PluginTest2

+ (void)load {
    [[FPluginCenter sharedInstance] registerPlugin:self];
}

+ (NSString *)identifier {
    return @"PluginTest2";
}

+ (NSArray<NSString *> *)hostIdentifiers {
    return @[@"HostTest", @"PluginVC"];
}

- (BOOL)plg_load:(FPluginArg *)arg {
    [arg.tableView plg_beginUpdate];
    [arg.tableView.plg_datas insertObject:@201 atIndex:1];
    arg.tableView.delegate = self;
    [arg.tableView plg_endUpdate];
    return YES;
}

- (void)plg_refresh:(FPluginArg *)arg {
    
}


- (void)plg_unLoad {
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
