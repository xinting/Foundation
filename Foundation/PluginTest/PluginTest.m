//
//  PluginTest.m
//  Foundation
//
//  Created by 新庭 on 2019/7/8.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import "PluginTest.h"

@interface PluginTest () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation PluginTest

+ (void)load {
    [[FPluginCenter sharedInstance] registerPlugin:self];
}

+ (id<FPlugin>)plg_instance {
    return [[PluginTest alloc] init];
}

+ (NSArray<NSString *> *)hostIdentifiers {
    return @[@"PluginVC", @"Application"];
}

+ (NSString *)identifier {
    return @"PluginTest";
}

- (BOOL)plg_load:(FPluginArg *)arg {
    if ([arg.hostIdentifier isEqualToString:@"Application"]) {
        return YES;
    }
    [arg.tableView plg_beginUpdate];
    [arg.tableView.plg_datas insertObjects:@[@101, @102, @103, @104] atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, 4)]];
    [arg.tableView plg_endUpdate];
    
    arg.tableView.delegate = self;
    arg.tableView.dataSource = self;
    
    [_PP(PluginTestInterface) print];
    return YES;
}

- (void)plg_refresh:(FPluginArg *)arg {
    
}

- (void)plg_unLoad {
    
}

- (NSArray<Protocol *> *)pluginBind {
    return @[
        @protocol(PluginTestInterface)
    ];
}

- (id)pluginCall:(FPluginCallCommend *)cmd {
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 30.f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.class identifier];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return [[self.class identifier] stringByAppendingString:@" Footer"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *number = [tableView.plg_datas objectAtIndex:indexPath.row];
    NSLog(@"Select %@", number);
}

@end
