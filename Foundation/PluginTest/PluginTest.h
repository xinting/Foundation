//
//  PluginTest.h
//  Foundation
//
//  Created by 新庭 on 2019/7/8.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FFoundation/FPluginHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PluginTestInterface <NSObject>

- (void)print;

@end

@interface PluginTest : NSObject <FPluginCallable, PluginTestInterface>

@end

NS_ASSUME_NONNULL_END
