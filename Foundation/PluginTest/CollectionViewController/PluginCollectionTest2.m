//
//  PluginCollectionTest2.m
//  Foundation
//
//  Created by 三井 on 2020/1/6.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "PluginCollectionTest2.h"

@implementation PluginCollectionTest2

+ (void)load {
    [[FPluginCenter sharedInstance] registerPlugin:self];
}

+ (NSArray<NSString *> *)hostIdentifiers {
    return @[@"PluginCollectionVC-PluginCollectionTest"];
}

+ (NSString *)identifier {
    return @"PluginCollectionVC-PluginCollectionTest-PluginCollectionTest2";
}

- (BOOL)plg_load:(FPluginArg *)arg {
    [arg.collectionView registerClass:UICollectionViewCell.class forCellWithReuseIdentifier:[self.class identifier]];
    arg.collectionView.dataSource = self;
    
    [arg.collectionView plg_beginUpdate];
    [arg.collectionView.plg_datas addObject:@1001];
    [arg.collectionView plg_endUpdate];
    
    return YES;
}

- (void)plg_refresh:(FPluginArg *)arg {
    
}

- (void)plg_unLoad {
    
}

#pragma mark - DataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    id data = [collectionView objectAtIndexPath:indexPath];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self.class identifier] forIndexPath:indexPath];
    cell.backgroundColor = [UIColor yellowColor];
    return cell;
}

@end
