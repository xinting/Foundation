//
//  PluginCollectionTest3.m
//  Foundation
//
//  Created by 三井 on 2020/1/14.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "PluginCollectionTest3.h"

@implementation PluginCollectionTest3

+ (void)load {
    [[FPluginCenter sharedInstance] registerPlugin:self];
}

+ (NSString *)identifier {
    return @"PluginCollectionTest3";
}

+ (NSArray<NSString *> *)hostIdentifiers {
    return @[@"PluginCollectionVC"];
}

- (BOOL)plg_load:(FPluginArg *)arg {
    arg.collectionView.dataSource = self;
    arg.collectionView.delegate = self;
    [arg.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass(self.class)];
    
    [arg.collectionView plg_beginUpdate];
    
    [arg.collectionView.plg_datas insertObject:self atIndex:0];
    
    [arg.collectionView plg_endUpdate];
    
    return YES;
}

- (void)plg_refresh:(FPluginArg *)arg {
    
}

- (void)plg_unLoad {
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return FPluginSectionEntire;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return FPluginNumberOfItemsInSection;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    id data = [collectionView objectAtIndexPath:indexPath];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self.class identifier] forIndexPath:indexPath];
    cell.backgroundColor = [UIColor cyanColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(345, 140);
//    return CGSizeZero;
}

@end
