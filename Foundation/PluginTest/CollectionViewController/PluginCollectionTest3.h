//
//  PluginCollectionTest3.h
//  Foundation
//
//  Created by 三井 on 2020/1/14.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPluginHeaders.h"

NS_ASSUME_NONNULL_BEGIN

@interface PluginCollectionTest3 : NSObject <FPlugin, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@end

NS_ASSUME_NONNULL_END
