//
//  PluginCollectionTest.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import "PluginCollectionTest.h"
#import <FFoundation/FPluginHeaders.h>

@implementation PluginCollectionTest

+ (void)load {
    [[FPluginCenter sharedInstance] registerPlugin:self];
}

+ (NSString *)identifier {
    return @"PluginCollectionTest";
}

+ (NSArray<NSString *> *)hostIdentifiers {
    return @[@"PluginCollectionVC"];
}

- (BOOL)plg_load:(FPluginArg *)arg {
    [arg.collectionView registerClass:UICollectionViewCell.class forCellWithReuseIdentifier:[self.class identifier]];
    arg.collectionView.dataSource = self;
    arg.collectionView.delegate = self;
    
    [arg.collectionView.plg_datas addObject:@102];
    [arg.collectionView.plg_datas addObject:@103];
    [arg.collectionView plghost_load:[FPluginArg argWithBuilder:^(FPluginArg *inarg) {
        inarg.hostIdentifier = @"PluginCollectionVC-PluginCollectionTest";
        inarg.collectionView = arg.collectionView;
    }]];
    NSArray *arr = arg.collectionView.plg_datas;
    
    [arg.collectionView plg_beginUpdate];
    
    [arg.collectionView.plg_datas insertObjects:arr atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, arr.count)]];
    
    [arg.collectionView plg_endUpdate];
    
    return YES;
}

- (void)plg_refresh:(FPluginArg *)arg {
    
}

- (void)plg_unLoad {
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    id data = [collectionView objectAtIndexPath:indexPath];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self.class identifier] forIndexPath:indexPath];
    cell.backgroundColor = [UIColor blueColor];
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(345, 140);
}

@end
