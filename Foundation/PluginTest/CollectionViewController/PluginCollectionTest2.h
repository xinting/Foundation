//
//  PluginCollectionTest2.h
//  Foundation
//
//  Created by 三井 on 2020/1/6.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FFoundation/FPluginHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@interface PluginCollectionTest2 : NSObject <FPlugin, UICollectionViewDataSource>

@end

NS_ASSUME_NONNULL_END
