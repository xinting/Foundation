//
//  PluginCollectionTest.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPluginHeaders.h"

NS_ASSUME_NONNULL_BEGIN

@interface PluginCollectionTest : NSObject <FPlugin, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@end

NS_ASSUME_NONNULL_END
