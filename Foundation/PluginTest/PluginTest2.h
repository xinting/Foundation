//
//  PluginTest2.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FFoundation/FPluginHeaders.h>

NS_ASSUME_NONNULL_BEGIN

@interface PluginTest2 : NSObject <FPlugin, UITableViewDelegate>

@end

NS_ASSUME_NONNULL_END
