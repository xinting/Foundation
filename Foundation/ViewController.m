//
//  ViewController.m
//  Foundation
//
//  Created by 吴新庭 on 2019/4/1.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import "ViewController.h"
#import <RPipeline+Filter.h>
#import <RPipeline+Input.h>
#import "ImageData.h"
#import <PWebViewController.h>
#import <RColorPicker.h>
#import <Masonry.h>
#import <FPDFViewController.h>
#import "MaskViewController.h"
#import <FFoundation/FMacroDefine.h>
#import <FFoundation/FXCategories.h>
#import <JKCategories/JKCategories.h>
#import "PipelineViewController.h"
#import <FFoundation/FTestHeaders.h>
#import <FFoundation/FPluginHeaders.h>

@interface ViewController () <RPipelineFilterProtocol, RPipelineInputProtocol, FPluginCenterDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) RColorPicker *colorPicker;
@property (nonatomic, strong) FPDFViewController *qlPreview;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [FPluginCenter sharedInstance].delegate = self;
    
    F_macro_msgSend(NSInteger, self, @selector(add::), @1, @2);
    
    NSString *key = @"rHAk2fgM6DPgiN88";
    NSData *data = [@"wuxinting" dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str = [[data AES:key] jk_base64EncodedString];
    NSLog(@"%@ %@", str, [@"wuxinting" jk_encryptedWithAESUsingKey:key andIV:nil]);
    
    FTestItem *item = [[FTestItem alloc] init];
    item.title = @"input bar";
    item.klass = NSClassFromString(@"InputBarViewController");
    [self.items addObject:item];
    
    [self.items addObject:[FTestItem objectWithBuilder:^(__kindof FTestItem * _Nonnull object) {
        object.title = @"PluginViewController";
        object.klass = NSClassFromString(@"PluginViewController");
    }]];
    [self.items addObject:[FTestItem objectWithBuilder:^(__kindof FTestItem * _Nonnull object) {
        object.title = @"PluginCollectionViewController";
        object.klass = NSClassFromString(@"PluginCollectionViewController");
        
        object.buildViewController = ^UIViewController * _Nonnull{
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PluginCollectionViewController"];
            return vc;
        };
    }]];
    [self.items addObject:kxBuildObject(FTestItem, {
        object.title = @"Pipeline";
        object.klass = NSClassFromString(@"PipelineViewController");
    })];
    [self.items addObject:kxBuildObject(FTestItem, {
        object.title = @"Audio";
        object.klass = NSClassFromString(@"AudioViewController");
    })];
    [self.items addObject:kxBuildObject(FTestItem, {
        object.title = NSLocalizedString(@"in_entrance", nil);
        object.klass = NSClassFromString(@"InternationalViewController");
    })];
    [self.items addObject:kxBuildObject(FTestItem, {
            object.title = @"Category";
            object.klass = NSClassFromString(@"CategoryViewController");
    })];
    [self.items addObject:kxBuildObject(FTestItem, {
            object.title = @"Algorithm";
            object.klass = NSClassFromString(@"AlgorithmViewController");
    })];
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"Crash";
        item.klass = NSClassFromString(@"CrashViewController");
    }];
}

- (NSInteger)add:(NSNumber *)a :(NSNumber *)b {
    NSInteger rs = a.integerValue + b.integerValue;
    NSLog(@"+++ Get %@", @(rs));
    return rs;
}

- (BOOL)shouldLoad:(NSString *)identifier forHost:(NSString *)hostIdentifier {
    if ([identifier isEqualToString:@"SubInputBarPlugin1"] && [hostIdentifier isEqualToString:@"SubInputBar"]) {
        return YES;
    }
    return YES;
}

#pragma mark - Filter
- (NSObject *)pipelineFilter:(NSObject *)object {
    if ([object isKindOfClass:ImageData.class]) {
        ImageData *img = (ImageData *)object;
        if (img.image) {
            return object;
        }
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:img.path]];
            img.image = [UIImage imageWithData:imgData];
            [object loose];
        });
        [object hold];
        return nil;
    }
    return object;
}

- (NSObject *)produceFor:(RPipeline *)pipeline {
    static NSUInteger index = 0;
    NSArray *arr = @[
                     @"https://sta-op-test.douyucdn.cn/nggsys/2019/01/02/07c5e49df79f4a7ead8c3719e62de0a2.jpg",
                     @"https://rpiclive.dz11.com/live-cover/appCovers/2018/12/04/77594443_20181204211353_small.jpg",
                     @"https://rpiclive.dz11.com/live-cover/appCovers/2018/12/04/77594442_20181204211150_small.jpg",
                     ];
    ImageData *img = [[ImageData alloc] init];
    img.path = [arr objectAtIndex:index++ % arr.count];
    sleep(1);
    NSLog(@"+++++++++取图%@++++++++++", img.path);
    return img;
}

- (IBAction)clickWebKit:(id)sender {
    PWebViewController *vc = [[PWebViewController alloc] init];
    vc.title = @"有钱花";
    vc.url = [NSURL URLWithString:@"http://10.112.23.164:8887/huankuanjihua.html"];
    [vc refresh];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)clickColorPick:(UIButton *)sender {
    if (self.colorPicker) {
        [self.colorPicker removeFromSuperview];
        self.colorPicker = nil;
        return;
    }
    RColorPicker *cp = [[RColorPicker alloc] init];
    [self.view addSubview:cp];
    [cp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(sender.mas_bottom);
        make.centerX.mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(300, 300));
    }];
    self.colorPicker = cp;
    [cp addTarget:self action:@selector(colorChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)colorChanged:(RColorPicker *)cp {
    self.view.backgroundColor = self.colorPicker.color;
}

- (IBAction)clickMask:(id)sender {
    MaskViewController *vc = [[MaskViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)clickShowPDF:(id)sender {
    FPDFViewController *vc = [[FPDFViewController alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"约翰·汤普森(1)" withExtension:@"pdf"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)clickPipeline:(id)sender {
    PipelineViewController *vc = [[PipelineViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
