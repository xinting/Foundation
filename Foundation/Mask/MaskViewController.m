//
//  MaskViewController.m
//  Foundation
//
//  Created by 三井 on 2019/10/25.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import "MaskViewController.h"
#import <FFoundation/FView.h>
#import <JKCategories/JKUIKit.h>
#import <Masonry/Masonry.h>

@interface MaskViewController ()

@property (nonatomic, strong) FMaskView *mask;

@end

@implementation MaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    
    self.mask = [[FMaskView alloc] initWithFrame:self.view.bounds];
//    self.view.maskView = self.mask;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    [self.mask gradientColor:@[[UIColor clearColor], [UIColor whiteColor]] locations:@[@0, @1] from:CGPointMake(0.5, 0) to:CGPointMake(0.5, 1)];
//    [self.mask gradientAlpha:@[@0, @1] direction:(CGVector){0, -1}];
    [self.view.mask shapeCircle:100 center:CGPointMake(0.5, 0.5) animated:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.view.mask shapeAll:YES];
    });
}

@end
