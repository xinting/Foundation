//
//  FAudioAnalyzer.h
//  FFoundation
//
//  Created by 新庭 on 2020/8/1.
//

#import <Foundation/Foundation.h>
@import AVFoundation;

NS_ASSUME_NONNULL_BEGIN

/// 音频分析
@interface FAudioAnalyzer : NSObject

/// 分析缓冲数据
/// @param buffer 缓冲数据
- (NSArray<NSArray<NSNumber *> *> *)analyse:(AVAudioPCMBuffer *)buffer;

@end

NS_ASSUME_NONNULL_END
