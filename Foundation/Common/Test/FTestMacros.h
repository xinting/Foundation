//
//  FTestMacros.h
//  FFoundation
//
//  Created by 三井 on 2021/6/8.
//

#ifndef FTestMacros_h
#define FTestMacros_h

#define FTestLogTag \
@"FTestLog"

#define FTestLogTag_Error \
FTestLogTag @" 🚒"

#define FTestLogTag_Warn \
FTestLogTag @" 🚓"

#define FTestLogTag_Info \
FTestLogTag @" 🚕"

#define FTestLogTag_Debug \
FTestLogTag @" 🚲"

#define FTestLogMark \
FTestLogMsg(FTestLogTag_Info, nil)

#define FTestLogDebug(format, ...) \
FTestLogMsg(FTestLogTag_Debug, ([NSString stringWithFormat:format, __VA_ARGS__]))

#define FTestLogInfo(format, ...) \
FTestLogMsg(FTestLogTag_Info, ([NSString stringWithFormat:format, __VA_ARGS__]))

#define FTestLogWarn(format, ...) \
FTestLogMsg(FTestLogTag_Warn, ([NSString stringWithFormat:format, __VA_ARGS__]))

#define FTestLogError(format, ...) \
FTestLogMsg(FTestLogTag_Error, ([NSString stringWithFormat:format, __VA_ARGS__]))

#define FTestLogMsg(_tag_, _msg_) \
do { int __line__ = __LINE__;\
    NSString *_file_name_ = [NSString stringWithFormat:@"%s", __FILE_NAME__];\
    _file_name_ = [_file_name_ stringByDeletingPathExtension];\
    NSString *_logStr_ = [NSString stringWithFormat:_tag_ @": Mark %@ %d ", _file_name_, __line__];\
    NSString *__msg__ = _msg_;\
    if (__msg__) { _logStr_ = [_logStr_ stringByAppendingString:__msg__];}\
    NSLog(@"%@", _logStr_);\
} while (0);

#endif /* FTestMacros_h */
