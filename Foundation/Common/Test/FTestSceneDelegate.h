//
//  FTestSceneDelegate.h
//  FFoundation
//
//  Created by 三井 on 2019/11/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

UIKIT_EXTERN API_AVAILABLE(ios(13.0)) @interface FTestSceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

NS_ASSUME_NONNULL_END
