//
//  FTestAppDelegate.m
//  FFoundation
//
//  Created by 三井 on 2019/11/12.
//

#import "FTestAppDelegate.h"
#import "FTestViewController.h"
#import <UIKit/UIKit.h>

@interface FTestAppDelegate ()

@end

@implementation FTestAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    
    Class root = [FTestViewController class];
    Class naviClass = [UINavigationController class];
    unsigned int count;
    Class *classes = objc_copyClassList(&count);
    for (int i = 0; i < count; i++) {
        Class klass = classes[i];

        if (class_conformsToProtocol(klass, @protocol(FTestEntrance)) && [klass isSubclassOfClass:root] && ![klass isEqual:root]) {
            root = klass;
        }
        
        if (class_conformsToProtocol(klass, @protocol(FTestEntrance)) && [klass isSubclassOfClass:naviClass] && ![klass isEqual:root]) {
            naviClass = klass;
        }
    }
    
    UINavigationController *navi = [[naviClass alloc] initWithRootViewController:[[root alloc] init]];
    self.window.rootViewController = navi;
    
    [self.window makeKeyAndVisible];
    return YES;
}
@end
