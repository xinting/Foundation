//
//  FTestItem.m
//  FFoundation
//
//  Created by 三井 on 2019/11/12.
//

#import "FTestItem.h"
#import "FTestMacros.h"

@implementation FTestItem

- (void)dealloc
{
    if (self.debug) {
        FTestLogDebug(@"FTestItem %@ Dealloc", self.title);
    }
}

@end
