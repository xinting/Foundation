//
//  FTestItem.h
//  FFoundation
//
//  Created by 三井 on 2019/11/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FTestItem : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) Class klass;  ///< target view controller
@property (nonatomic, copy) UIViewController * (^buildViewController)(void); ///< return view controller instance, if it's not null, ignore \c klass
@property (nonatomic, copy) void (^action)(void); ///< action when `did select` event occur

@property (nonatomic, assign) BOOL debug;

@end

NS_ASSUME_NONNULL_END
