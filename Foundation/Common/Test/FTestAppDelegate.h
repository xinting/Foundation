//
//  FTestAppDelegate.h
//  FFoundation
//
//  Created by 三井 on 2019/11/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FTestAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

NS_ASSUME_NONNULL_END
