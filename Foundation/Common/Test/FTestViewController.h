//
//  FTestViewController.h
//  FFoundation
//
//  Created by 三井 on 2019/11/12.
//

#import <UIKit/UIKit.h>
#import "FTestItem.h"
#import <FFoundation/PBaseViewController.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FTestEntrance <NSObject>
@end

@interface FTestViewController : PBaseViewController <UITableViewDelegate, UITableViewDataSource, NSObject>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<FTestItem *> *items;

/// 显示在表头的信息区
@property (nonatomic, copy) NSString *info;

- (void)addItem:(void (NS_NOESCAPE ^)(FTestItem *item))build;

@end

NS_ASSUME_NONNULL_END
