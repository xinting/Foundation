//
//  FLayoutItem.h
//  FFoundation
//
//  Created by 三井 on 2019/11/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FLayoutItem : NSObject

@property (nonatomic, assign) CGFloat height;

#pragma mark - Control
@property (nonatomic, assign) BOOL deleteable;      ///< default is NO
@property (nonatomic, assign) BOOL insertable;      ///< default is NO
@property (nonatomic, assign) BOOL editable;        ///< default is NO
@property (nonatomic, assign) BOOL enable;          ///< default is YES

@end

NS_ASSUME_NONNULL_END
