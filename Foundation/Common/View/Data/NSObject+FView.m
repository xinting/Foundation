//
//  NSObject+FView.m
//  FFoundation
//
//  Created by 三井 on 2019/11/6.
//

#import "NSObject+FView.h"
#import <objc/runtime.h>

static const void *kObjectLayoutItemKey = &kObjectLayoutItemKey;
@implementation NSObject (FView)

- (FLayoutItem *)layoutItem {
    FLayoutItem *item = objc_getAssociatedObject(self, kObjectLayoutItemKey);
    if (!item) {
        item = [[FLayoutItem alloc] init];
        objc_setAssociatedObject(self, kObjectLayoutItemKey, item, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return item;
}

@end
