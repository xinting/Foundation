//
//  FSeparatorItem.m
//  FFoundation
//
//  Created by 三井 on 2019/11/4.
//

#import "FSeparatorItem.h"

@implementation FSeparatorItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        _color = [UIColor lightGrayColor];
        _width = 0.5f;
        _style = FSeparatorStyleLine;
    }
    return self;
}

@end
