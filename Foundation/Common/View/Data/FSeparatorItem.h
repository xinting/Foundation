//
//  FSeparatorItem.h
//  FFoundation
//
//  Created by 三井 on 2019/11/4.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FSeparatorStyle) {
    FSeparatorStyleNone                   = 0,
    FSeparatorStyleLine                   = 1,
};

NS_ASSUME_NONNULL_BEGIN

@interface FSeparatorItem : NSObject

@property (nonatomic, assign) FSeparatorStyle style;
@property (nonatomic, assign) UIRectEdge edge;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, assign) UIEdgeInsets insets;
@property (nonatomic, assign) CGFloat width;

@end

NS_ASSUME_NONNULL_END
