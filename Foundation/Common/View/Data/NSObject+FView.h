//
//  NSObject+FView.h
//  FFoundation
//
//  Created by 三井 on 2019/11/6.
//

#import <Foundation/Foundation.h>
#import "FLayoutItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (FView)

@property (nonatomic, strong, readonly) FLayoutItem *layoutItem;

@end

NS_ASSUME_NONNULL_END
