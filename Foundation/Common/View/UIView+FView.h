//
//  UIView+FView.h
//  Foundation
//
//  Created by 新庭 on 2019/10/24.
//

#import <UIKit/UIKit.h>
#import "FMaskView.h"

NS_ASSUME_NONNULL_BEGIN

typedef NSInteger UIViewReturnIndex;
typedef void (^UIViewReturn)(UIView *v, UIViewReturnIndex index);
UIKIT_EXTERN const UIViewReturnIndex UIViewReturnIndexYes;
UIKIT_EXTERN const UIViewReturnIndex UIViewReturnIndexNo;

@interface UIView (FView)

@property (nonatomic, strong, readonly) FMaskView *mask;    ///< MaskView, cover with .maskView
@property (nonatomic, copy) UIViewReturn onReturn;  ///< return result block

- (void)prepareSubviews;
- (void)refresh;

- (void)cleanSubviews;

/// 切换显示隐藏
- (void)toggleHidden;

/// 添加横线。颜色为layer.borderColor
/// @param width 线宽度
/// @param insets insets
/// @param edge 在哪边
/// @return line
- (UIView *)addLine:(CGFloat)width insets:(UIEdgeInsets)insets edge:(UIRectEdge)edge;

@end

NS_ASSUME_NONNULL_END
