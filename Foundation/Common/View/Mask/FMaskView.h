//
//  FMaskView.h
//  Foundation
//
//  Created by 三井 on 2019/10/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FMaskView : UIView

/// the view which mask for
@property (nonatomic, weak) UIView *maskFor;

/// stop mask, remove self from the view mask for
- (void)maskOff;

/// 设置渐变通透型的遮罩
/// @param colors 颜色
/// @param locations 位置
/// @param start 起始
/// @param end 结束
- (void)gradientColor:(NSArray<UIColor *> *)colors locations:(NSArray<NSNumber *> *)locations from:(CGPoint)start to:(CGPoint)end;
/// 0, 1 alpha
- (void)gradientAlpha:(NSArray<NSNumber *> *)locations direction:(CGVector)direction;

/// 显示整个蒙层
/// @param animate 是否动画显示
- (void)shapeAll:(BOOL)animate;
- (void)shapeCircel:(CGFloat)radius;    ///< 中心圆，无动画
- (void)shapeCircle:(CGFloat)radius center:(CGPoint)anchor; ///< 无动画

/// 以anchor为锚点，radius 为半径设置圆形显示区域
/// @param radius 半径
/// @param anchor 锚点
/// @param animate 是否有动画
- (void)shapeCircle:(CGFloat)radius center:(CGPoint)anchor animated:(BOOL)animate;

/// 切单个圆角
/// @param corner 圆角
/// @param radius 半径
- (void)shapeCorner:(UIRectCorner)corner radius:(CGFloat)radius;

/// 设置Path
/// @param duration duration
/// @param path path
/// @param animate 是否动画过渡
- (void)shapePath:(CGPathRef)path duration:(CGFloat)duration animated:(BOOL)animate;

@end

NS_ASSUME_NONNULL_END
