//
//  FFloatButton.h
//  Foundation
//
//  Created by 吴新庭 on 2019/9/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FFloatButton : UIButton

@property (nonatomic, assign, getter=isMoveable) BOOL moveable;

/// 如果不等于-1，停泊时的偏移量即为dockOffset
@property (nonatomic, assign) CGFloat dockOffset;

/// 自动dock的间隔时间，默认为2s，如果 <= 0，不自动dock
- (void)autoDockForView:(UIView *)view withInterval:(NSTimeInterval)interval;

- (void)dockForView:(UIView *)view;
- (void)undock;

@end

NS_ASSUME_NONNULL_END
