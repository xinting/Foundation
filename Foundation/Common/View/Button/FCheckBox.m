//
//  FCheckBox.m
//  FFoundation
//
//  Created by 三井 on 2020/9/25.
//

#import "FCheckBox.h"
#import "UIImage+FView.h"

@implementation FCheckBox

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImage *imgNormal = [[UIImage view_imageNamed:@"checkboxoutlineblank"] jk_imageScaledToSize:frame.size];
        UIImage *imgSel = [[UIImage view_imageNamed:@"checkboxoutline"] jk_imageScaledToSize:frame.size];
        [self setImage:imgNormal forState:UIControlStateNormal];
        [self setImage:imgSel forState:UIControlStateSelected];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self;
}

@end
