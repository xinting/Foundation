//
//  FInputBarTextField.m
//  FFoundation
//
//  Created by 新庭 on 2020/2/7.
//

#import "FInputBarTextField.h"
#import "FView.h"

@implementation FInputBarTextField

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self prepareSubviews];
    }
    return self;
}

- (void)prepareSubviews {
    [self addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(0);
    }];
}

- (void)setPadding:(UIEdgeInsets)padding {
    _padding = padding;
    
    [self.textField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(padding);
    }];
}

- (void)refresh {
}

#pragma mark - Private
@synthesize textField = _textField;
- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.borderStyle = UITextBorderStyleRoundedRect;
    }
    return _textField;
}

@end
