//
//  FInputBar+Plugin.m
//  FFoundation
//
//  Created by 新庭 on 2020/2/11.
//

#import "FInputBar+Plugin.h"
#import "NSObject+FPlugin.h"
#import "FInputBarHostProxy.h"
#import "FInputBarFake.h"
#import "FInputBarPluginHost.h"

@interface FInputBar ()

@end

@implementation FInputBar (Plugin)

- (FPluginHost *)hostFor:(FPluginArg *)arg {
    FInputBarPluginHost *host = [[FInputBarPluginHost alloc] initWithHost:arg.hostIdentifier];
    arg.collectable = self;
    return host;
}

- (FPluginHostProxy *)proxyFor:(FPluginArg *)arg {
    FInputBarHostProxy *proxy = [[FInputBarHostProxy alloc] initWithSource:arg.collectable];
    return proxy;
}

- (FPluginFake *)fakeForIdentifier:(NSString *)identifier {
    FInputBarFake *fake = [FInputBarFake fakeFor:self];
    return fake;
}

@end

@implementation FPluginArg (FInputBar)

- (void)setInputBar:(FInputBar *)inputBar {
    self.collectable = inputBar;
}

- (FInputBar *)inputBar {
    return self.collectable;
}

@end
