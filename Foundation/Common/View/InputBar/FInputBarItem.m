//
//  FInputBarItem.m
//  FFoundation
//
//  Created by 新庭 on 2020/2/5.
//

#import "FInputBarItem.h"

@interface FInputBarItem ()

@property (nonatomic, strong) UIView *view;

@end

@implementation FInputBarItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        _size = CGSizeMake(44, 44);
    }
    return self;
}

@end
