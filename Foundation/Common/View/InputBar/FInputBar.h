//
//  FInputBar.h
//  FFoundation
//
//  Created by 新庭 on 2020/2/5.
//

#import <UIKit/UIKit.h>
#import "FInputBarItem.h"
#import "FInputBarTextField.h"

NS_ASSUME_NONNULL_BEGIN

UIKIT_EXTERN CGFloat FInputBarPanelDefaultHeight;

@class FInputBar;
@protocol FInputBarDelegate <UITextFieldDelegate>

@required

/// 当渲染到具体item里的回调.如果返回为空，将忽略该item的渲染
/// @param bar 输入栏
/// @param item 渲染数据
- (UIView *)inputBar:(FInputBar *)bar willRenderItem:(FInputBarItem *)item;

@optional
- (void)inputBar:(FInputBar *)bar didRenderedItem:(FInputBarItem *)item withView:(UIView *)view;

- (void)inputBar:(FInputBar *)bar unfoldPanel:(UIView *)panel;
- (void)inputBar:(FInputBar *)bar foldPanel:(UIView *)panel;

- (void)inputBar:(FInputBar *)bar unfoldHead:(UIView *)head;
- (void)inputBar:(FInputBar *)bar foldHead:(UIView *)head;

@end

@interface FInputBar : UIView

@property (nonatomic, strong, readonly) FInputBarTextField *inputBarTextField;
@property (nonatomic, weak) id<FInputBarDelegate> delegate;

/// 内边距
@property (nonatomic, assign) UIEdgeInsets padding;

/// 渲染数据依据
@property (nonatomic, strong) NSMutableArray<FInputBarItem *> *items;

/// 输入框下方面板，可为空
@property (nonatomic, weak, readonly) UIView  * _Nullable panel;

/// 输入框上方头部视图，如果为空，则没有头部视图
@property (nonatomic, weak, readonly) UIView * _Nullable head;

/// 是否为向下展开状态。如果为默认键盘或自定义面板，将返回YES
@property (nonatomic, assign, readonly, getter=isUnfolded) BOOL unfolded;

- (void)refresh;

/// 展开或关闭输入框下方面板
/// @param panel 面板，为空时打开默认键盘
/// @param height 面板高度
- (void)unfoldPanel:(UIView * _Nullable)panel height:(CGFloat)height;
- (void)foldPanel;

/// 展开头部视图位于输入框上方
/// @param head 头部视图
/// @param height 高度
- (void)unfoldHead:(UIView *)head height:(CGFloat)height;
- (void)foldHead;

/// 返回item对应的视图，如果没有返回空
/// @param item Item
- (UIView *)viewForItem:(FInputBarItem *)item;
- (FInputBarItem *)itemForView:(UIView *)view;

@end

@interface FInputBar (SafeArea)

/// 添加输入框边距，适应安全区。如果需要设置 \c padding, 请在该方法之前设置
/// @param height 输入框的高度
- (void)safeAreaInsistBarHeight:(CGFloat)height;

@end
NS_ASSUME_NONNULL_END
