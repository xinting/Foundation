//
//  FInputBarItem.h
//  FFoundation
//
//  Created by 新庭 on 2020/2/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, FInputBarItemPosition) {
    FInputBarItemPositionLeft,
    FInputBarItemPositionInnerLeft,       ///< 位于输入框内部左侧
    FInputBarItemPositionRight
};

typedef NS_ENUM(NSInteger, FInputBarItemType) {
    FInputBarItemTypeDefault,   ///< 该item与视图一一对应
    FInputBarItemTypeStack,     ///< 该item为堆叠视图，渲染时将使用 \c subItems
};

@interface FInputBarItem : NSObject

/// item名称，也为标识，如果 \c type 标识为 FInputBarItemTypeStack，则与之同名的Item，按顺序渲染，取第一个非空的视图，其余忽略
@property (nonatomic, copy) NSString *name;

/// 该Item的类型
@property (nonatomic, assign) FInputBarItemType type;

/// item的位置，左、右
@property (nonatomic, assign) FInputBarItemPosition position;

/// 如果height为0，则贴紧上下边, width 不可为0; default is {44, 44}
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) UIEdgeInsets insets;


/// 方便业务使用
@property (nonatomic, assign, getter=isSelected) BOOL selected;

@end

NS_ASSUME_NONNULL_END
