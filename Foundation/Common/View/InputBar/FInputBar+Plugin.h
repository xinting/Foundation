//
//  FInputBar+Plugin.h
//  FFoundation
//
//  Created by 新庭 on 2020/2/11.
//

#import "FInputBar.h"
#import "FPluginHostable.h"
#import "NSObject+FPlugin.h"
#import "FPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface FInputBar (Plugin) <FPluginHostable>

@end

@interface FPluginArg (FInputBar)

@property (nonatomic, strong) FInputBar *inputBar;

@end

NS_ASSUME_NONNULL_END
