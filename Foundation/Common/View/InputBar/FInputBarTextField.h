//
//  FInputBarTextField.h
//  FFoundation
//
//  Created by 新庭 on 2020/2/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FInputBarTextField : UIView

@property (nonatomic, strong, readonly) UITextField *textField;

@property (nonatomic, assign) UIEdgeInsets padding;

@end

NS_ASSUME_NONNULL_END
