//
//  FInputBarFake.h
//  FFoundation
//
//  Created by 新庭 on 2020/2/11.
//

#import "FPluginFakeView.h"
#import "FInputBar.h"

NS_ASSUME_NONNULL_BEGIN

@interface FInputBarFake : FPluginFakeView

@property (nonatomic, weak) id<FInputBarDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
