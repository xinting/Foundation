//
//  FImageMarkButton.m
//  Foundation
//
//  Created by 新庭 on 2019/8/31.
//

#import "FImageMarkButton.h"
#import "UIImage+FView.h"

@interface FImageMarkButton ()

@property (nonatomic, strong) UIVisualEffectView *effectView;
@property (nonatomic, strong) UIView *shadowView;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation FImageMarkButton

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self jk_setRoundedCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft radius:self.jk_height / 2.f];
    self.shadowView.layer.cornerRadius = self.shadowView.jk_height / 2.f;
//    [self.imageView jk_setRoundedCorners:UIRectCornerAllCorners radius:self.imageView.jk_height / 2.f];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        self.shadowView.layer.shadowOpacity = 1.f;
        self.imageView.tintColor = [UIColor jk_colorWithHex:0x417505];
    } else {
        self.shadowView.layer.shadowOpacity = 0.f;
        self.imageView.tintColor = [UIColor jk_colorWithHex:0x9B9B9B];
    }
    
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

#pragma mark - Action
- (void)panHandle:(UIPanGestureRecognizer *)gr {
    CGPoint point = [gr translationInView:self];
    
    self.transform = CGAffineTransformTranslate(self.transform, 0, point.y);
    
    [gr setTranslation:CGPointZero inView:self];
}

#pragma mark - Private
- (void)setup {
    [self addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandle:)]];
    
    self.effectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    self.effectView.userInteractionEnabled = NO;
    [self addSubview:self.effectView];
    [self.effectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(0);
    }];
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = NO;
    self.shadowView.backgroundColor = [UIColor jk_colorWithHex:0xEBEBEB];
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowColor = [UIColor jk_colorWithHex:0x417505].CGColor;
    self.shadowView.layer.shadowRadius = 2.f;
    self.shadowView.layer.shadowOpacity = 0.f;
    [self addSubview:self.shadowView];
    [self.shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.mas_offset(3);
        make.bottom.mas_offset(-3);
        make.trailing.mas_offset(-11);
        make.width.mas_equalTo(self.shadowView.mas_height);
    }];
    
    self.imageView = [[UIImageView alloc] initWithImage:[[UIImage view_imageNamed:@"view-pen"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    self.imageView.userInteractionEnabled = NO;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.tintColor = [UIColor jk_colorWithHex:0x9B9B9B];
    [self.shadowView addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.shadowView);
        make.leading.top.mas_offset(8);
        make.bottom.trailing.mas_offset(-8);
    }];
}

@end

