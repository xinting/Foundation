//
//  FImageEditViewController.h
//  Foundation
//
//  Created by 吴新庭 on 2019/8/30.
//

#import "PBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FImageEditViewController : PBaseViewController

@property (nonatomic, strong) UIImage *image;

@end

NS_ASSUME_NONNULL_END
