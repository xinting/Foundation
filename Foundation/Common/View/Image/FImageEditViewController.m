//
//  FImageEditViewController.m
//  Foundation
//
//  Created by 吴新庭 on 2019/8/30.
//

#import "FImageEditViewController.h"
#import "UIImage+FView.h"
#import "FImageMarkButton.h"
#import "FDrawableImageView.h"

@interface FImageEditViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *zoomView;
@property (nonatomic, strong) FDrawableImageView *imgView;

@property (nonatomic, strong) FImageMarkButton *markButton;

@end

@implementation FImageEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage view_imageNamed:@"view-share"] jk_imageScaledToSize:CGSizeMake(32, 32)] style:UIBarButtonItemStylePlain target:self action:@selector(shareImage:)];
    
    [self.view addSubview:self.zoomView];
    [self.zoomView addSubview:self.imgView];
    self.zoomView.delegate = self;
    [self.zoomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(0);
    }];
    
    [self.view addSubview:self.markButton];
    [self.markButton addTarget:self action:@selector(clickMark:) forControlEvents:UIControlEventTouchUpInside];
    [self.markButton addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)]];
    [self.markButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_offset(0);
        make.trailing.mas_offset(0);
        make.height.mas_equalTo(44);
    }];
}

- (void)setImage:(UIImage *)image {
    _image = image;
    
    self.imgView.image = image;
    
    self.zoomView.minimumZoomScale = MIN(self.view.jk_height / image.size.height, self.view.jk_width / image.size.width);
    self.zoomView.contentSize = image.size;
}

#pragma mark - Action
- (void)shareImage:(id)sender {
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:@[self.imgView.drawedImage] applicationActivities:nil];
    [self presentViewController:activity animated:YES completion:nil];
}

- (void)clickMark:(FImageMarkButton *)button {
    button.selected = !button.selected;
    self.zoomView.scrollEnabled = !button.selected;
    self.imgView.edit = button.selected;
}

- (void)longPress:(FImageMarkButton *)button {
    [self.imgView clean];
    
    if (@available(iOS 10.0, *)) {
        UIImpactFeedbackGenerator *generator = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight];
        [generator prepare];
        [generator impactOccurred];
    } else {
        // Fallback on earlier versions
    }
}

#pragma mark - Scroll View
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imgView;
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    
}

#pragma mark - Private
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [[FDrawableImageView alloc] initWithImage:self.image];
    }
    return _imgView;
}

- (UIScrollView *)zoomView {
    if (!_zoomView) {
        _zoomView = [[UIScrollView alloc] init];
        _zoomView.minimumZoomScale = 0.5f;
        _zoomView.maximumZoomScale = 3.f;
    }
    return _zoomView;
}

- (FImageMarkButton *)markButton {
    if (!_markButton) {
        _markButton = [[FImageMarkButton alloc] init];
    }
    return _markButton;
}

@end
