//
//  FDrawView.h
//  Foundation
//
//  Created by 吴新庭 on 2019/9/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FDrawContent : NSObject

@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic, strong) UIColor *color;

@end

@interface FDrawView : UIView

@property (nonatomic, assign) BOOL drawable;

@property (nonatomic, strong, readonly) UIImage *snapImage;
@property (nonatomic, strong) UIColor *paintColor;
@property (nonatomic, assign) CGFloat paintWidth;

- (void)clean;

@end

NS_ASSUME_NONNULL_END
