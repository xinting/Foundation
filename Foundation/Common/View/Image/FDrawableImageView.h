//
//  FDrawableImageView.h
//  Foundation
//
//  Created by 吴新庭 on 2019/9/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FDrawableImageView : UIImageView

@property (nonatomic, assign, getter=isEditing) BOOL edit;
@property (nonatomic, strong, readonly) UIImage *drawedImage;

- (void)clean;
@end

NS_ASSUME_NONNULL_END
