//
//  UIView+FView.m
//  Foundation
//
//  Created by 新庭 on 2019/10/24.
//

#import "UIView+FView.h"
#import "FMaskView.h"
#import "FMacroDefine.h"

static NSInteger kLineTagBottom = 1024;
static NSInteger kLineTagRight = 1025;
static NSInteger kLineTagLeft = 1026;
static NSInteger kLineTagTop = 1027;
static NSInteger kMaskTag   = 1028;

F_macro_property(kUIViewReturnBlock);
@implementation UIView (FView)

- (FMaskView *)mask {
    FMaskView *mv = nil;
    if ([self.maskView isKindOfClass:FMaskView.class]) {
        mv = (FMaskView *)self.maskView;
    } else if (self.maskView) {
        UIView *view = [self.maskView viewWithTag:kMaskTag];
        if ([view isKindOfClass:FMaskView.class]) {
            mv = (FMaskView *)view;
        }
    } else {
        FMaskView *fmask = [[FMaskView alloc] initWithFrame:self.bounds];
        fmask.tag = kMaskTag;
        fmask.maskFor = self.maskView ?: self;
        fmask.maskFor.maskView = fmask;
        mv = fmask;
    }
    if (!CGRectEqualToRect(mv.frame, self.bounds)) {
        mv.frame = self.bounds;
    }
    return mv;
}

- (UIViewReturn)onReturn {
    return objc_getAssociatedObject(self, kUIViewReturnBlockKey);
}

- (void)setOnReturn:(UIViewReturn)onReturn {
    objc_setAssociatedObject(self, kUIViewReturnBlockKey, onReturn, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void)prepareSubviews {}
- (void)refresh {}

- (void)cleanSubviews {
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
}

- (void)toggleHidden {
    self.hidden = !self.isHidden;
}

- (UIView *)addLine:(CGFloat)width insets:(UIEdgeInsets)insets edge:(UIRectEdge)edge {
    UIView *line = [self lineForEdge:edge];
    switch (edge) {
        case UIRectEdgeBottom:
            [line mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_offset(0);
                make.left.mas_offset(insets.left);
                make.right.mas_offset(-insets.right);
                make.height.mas_equalTo(width);
            }];
            break;
        case UIRectEdgeRight:
            [line mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_offset(insets.bottom);
                make.top.mas_offset(insets.top);
                make.right.mas_offset(insets.right);
                make.width.mas_offset(width);
            }];
            break;
            
        case UIRectEdgeLeft:
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_offset(insets.top);
                make.bottom.mas_offset(insets.bottom);
                make.left.mas_offset(insets.left);
                make.width.mas_equalTo(width);
            }];
            break;
            
        case UIRectEdgeTop:
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_offset(insets.top);
                make.left.mas_offset(insets.left);
                make.right.mas_offset(insets.right);
                make.height.mas_equalTo(width);
            }];
            break;
            
        default:
            break;
    }
    return line;
}

#pragma mark - Private
- (UIView *)lineForEdge:(UIRectEdge)edge {
    UIView *line = nil;
    NSInteger tag = 0;
    switch (edge) {
        case UIRectEdgeBottom:
            tag = kLineTagBottom;
            break;
            
        case UIRectEdgeRight:
            tag = kLineTagRight;
            break;
            
        case UIRectEdgeLeft:
            tag = kLineTagLeft;
            break;
            
        case UIRectEdgeTop:
            tag = kLineTagTop;
            break;
            
        default:
            break;
    }
    line = [self viewWithTag:tag];
    if (!line) {
        line = [[UIView alloc] init];
        [self addSubview:line];
    }
    line.tag = tag;
    line.backgroundColor = [UIColor colorWithCGColor:self.layer.borderColor];
    return line;
}

@end

const UIViewReturnIndex UIViewReturnIndexYes = YES;
const UIViewReturnIndex UIViewReturnIndexNo = NO;
