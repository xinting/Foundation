//
//  ListItem.m
//  Foundation
//
//  Created by 新庭 on 2019/6/2.
//

#import "ListItem.h"

@implementation ListItem

+ (ListItem *)itemWith:(NSObject *)item {
    ListItem *inst = [[ListItem alloc] init];
    inst.item = item;
    return inst;
}
@end
