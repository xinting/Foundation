//
//  ListItem.h
//  Foundation
//
//  Created by 新庭 on 2019/6/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ListItem : NSObject

@property (nonatomic, strong) NSObject *item;

+ (ListItem *)itemWith:(NSObject *)item;

@end

NS_ASSUME_NONNULL_END
