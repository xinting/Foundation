//
//  ListCellProtocol.h
//  Foundation
//
//  Created by 新庭 on 2019/6/2.
//

#ifndef ListCellProtocol_h
#define ListCellProtocol_h

#import "ListItem.h"

@protocol ListCellProtocol <NSObject>

@required
/**
 以列表项更新列表项视图

 @param item 列表项数据
 */
- (void)updateWithItem:(ListItem *)item;

@end

#endif /* ListCellProtocol_h */
