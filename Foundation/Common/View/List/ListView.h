//
//  ListView.h
//  Foundation
//
//  Created by 新庭 on 2019/6/2.
//

#import <UIKit/UIKit.h>
#import "ListCellProtocol.h"

NS_ASSUME_NONNULL_BEGIN

typedef UITableViewCell<ListCellProtocol> ListViewCell;

@interface ListView : UITableView

/// 设置被选择的列表项
@property (nonatomic, copy) NSArray<ListItem *> * items;
@property (nonatomic, assign) Class cellClass;

@property (nonatomic, strong) UIFont *font;

/**
 block格式化item为字符串，并显示在列表项视图中。
 如果设置了 ListCell，则忽略该格式化
 */
@property (nonatomic, copy) NSString * (^titleFormater)(ListItem *item);
@property (nonatomic, copy) void (^onSelect)(ListView *list);

@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, strong, readonly) ListItem *selectedItem;

/**
 初始化列表

 @param items 数据，可之后设置、更新
 @param cellClass 列表项视图承载的类，必须为 UITableViewCell<ListCellProtocol>子类。如果为nil，使用UITableViewCell
 @return 列表实例
 */
+ (ListView *)listViewWith:(nullable NSArray *)items andCell:(nullable Class)cellClass;

@end

NS_ASSUME_NONNULL_END
