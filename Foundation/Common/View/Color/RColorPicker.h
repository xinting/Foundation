//
//  RColorPicker.h
//  Pods
//
//  Created by 吴新庭 on 2019/8/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RColorPicker : UIControl

@property (nonatomic, strong) UIColor *color;

@end

NS_ASSUME_NONNULL_END
