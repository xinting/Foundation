//
//  RColorPicker.m
//  Pods
//
//  Created by 吴新庭 on 2019/8/28.
//

#import "RColorPicker.h"
#import "PDefine.h"
#import <MSColorPicker/MSColorWheelView.h>

@interface RColorPicker ()

@property (nonatomic, strong) MSColorWheelView *wheelColorView;

@end
@implementation RColorPicker

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        
        MSColorWheelView *cp = [[MSColorWheelView alloc] init];
        [self addSubview:cp];
        [cp mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_offset(0);
        }];
        self.wheelColorView = cp;
    }
    return self;
}

- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents {
    [self.wheelColorView addTarget:target action:action forControlEvents:controlEvents];
}

- (void)removeTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents {
    [self.wheelColorView removeTarget:target action:action forControlEvents:controlEvents];
}

- (UIColor *)color {
    return [UIColor colorWithHue:self.wheelColorView.hue saturation:self.wheelColorView.saturation brightness:1.0 alpha:1.0];
}

#pragma mark - Action

#pragma mark - Private
- (NSArray<NSArray<NSNumber *> *> *)colorMatrix {
    return @[
        @[@0, @-1, @0, @0, @1, @0], // r
        @[@1, @0, @0, @-1, @0, @0], // g
        @[@0, @0, @1, @0, @0, @-1]  // b
    ];
}

- (CGFloat)distanceFrom:(CGPoint)point1 to:(CGPoint)point2 {
    return sqrt((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y));
}

- (void)drawColorCircle:(CGContextRef)context inRect:(CGRect)rect {
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    CGFloat radius = MIN(rect.size.width, rect.size.height) / 2;
    
    NSArray<NSArray<NSNumber *> *> * cMatrix = [self colorMatrix];
    
    CGContextSetLineWidth(context, 0.5f);
    
    CGFloat r = 1.f, g = 0.f, b = 0.f;
    CGFloat step = 60.f / 255.f;
    
    for (NSInteger x = center.x - radius; x <= center.x + radius; x++) {
        for (NSInteger y = center.y - radius; y <= center.y + radius; y++) {
            CGFloat distance = [self distanceFrom:center to:CGPointMake(x, y)];
            if (distance > radius) continue;
            
            CGFloat ratio = distance / radius;
            CGFloat cosa = (y - center.y) / distance;
            
            NSInteger zone = 0;
            if (cosa < 1 && cosa > 0.5) {
                zone = 0;
            } else if (cosa <= 0.5 && cosa > -0.5) {
                zone = 1;
            } else if (cosa <= -0.5 && cosa > -1) {
                zone = 2;
            }
            r += cMatrix[0][zone].floatValue * step;
            g += cMatrix[1][zone].floatValue * step;
            b += cMatrix[2][zone].floatValue * step;
            
            UIColor *c = D_UI_COLOR_RGBA(r * ratio, g * ratio, b * ratio, 1.f);
            CGContextMoveToPoint(context, x, y);
            CGContextAddLineToPoint(context, x + 0.5, y + 0.5);
            CGContextSetStrokeColorWithColor(context, c.CGColor);
            CGContextStrokePath(context);
        }
    }
}

- (void)drawColorBar:(CGContextRef)context inRect:(CGRect)rect {
    CGContextSetLineWidth(context, 0.5f);

    CGFloat x = 0, y = 0;
    CGFloat length = 30;
    
    UInt32 color = 0x00FF00;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 255; j++) {
            UIColor *c = [UIColor jk_colorWithHex:color];
            CGContextMoveToPoint(context, x, y);
            CGContextAddLineToPoint(context, x++, y + length);
            CGContextSetStrokeColorWithColor(context, c.CGColor);
            CGContextStrokePath(context);
            
            UInt32 step = 0x1 << (i * 8);
            color += step;
        }
    }
}

@end
