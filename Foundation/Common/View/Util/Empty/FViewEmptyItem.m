//
//  FViewEmptyItem.m
//  Foundation
//
//  Created by 三井 on 2019/10/15.
//

#import "FViewEmptyItem.h"

@implementation FViewEmptyItem

- (FViewEmptyContainer *)container {
    if (!_container) {
        _container = [[FViewEmptyContainer alloc] init];
    }
    return _container;
}

@end
