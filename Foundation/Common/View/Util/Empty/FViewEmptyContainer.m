//
//  FViewEmptyContainer.m
//  Foundation
//
//  Created by 三井 on 2019/10/15.
//

#import "FViewEmptyContainer.h"

@interface FViewEmptyContainer ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *infoLabel;

@property (nonatomic, strong) UITapGestureRecognizer *tapGR;

@end

@implementation FViewEmptyContainer

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.imageView];
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_greaterThanOrEqualTo(self);
            make.leading.mas_greaterThanOrEqualTo(self);
            make.trailing.mas_lessThanOrEqualTo(self);
            make.centerX.mas_offset(0);
            make.width.height.mas_equalTo(80);
        }];
        
        [self addSubview:self.infoLabel];
        [self.infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.mas_greaterThanOrEqualTo(self);
            make.trailing.mas_lessThanOrEqualTo(self);
            make.top.mas_equalTo(self.imageView.mas_bottom).mas_offset(10);
            make.centerX.mas_offset(0);
        }];
        
        [self addSubview:self.controlButton];
        [self.controlButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.infoLabel.mas_bottom);
            make.centerX.mas_offset(0);
            make.bottom.mas_lessThanOrEqualTo(self);
        }];
    }
    return self;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    
    self.imageView.image = image;
}

- (void)setInfo:(NSString *)info {
    _info = info;
    
    self.infoLabel.text = info;
}

#pragma mark - Private
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
    }
    return _imageView;
}

- (UILabel *)infoLabel {
    if (!_infoLabel) {
        _infoLabel = [[UILabel alloc] init];
        _infoLabel.textColor = [UIColor lightGrayColor];
        _infoLabel.font = [UIFont systemFontOfSize:12];
    }
    return _infoLabel;
}

- (UIButton *)controlButton {
    if (!_controlButton) {
        _controlButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _controlButton.titleLabel.font = [UIFont systemFontOfSize:12];
        _controlButton.hidden = YES;
    }
    return _controlButton;
}

@end
