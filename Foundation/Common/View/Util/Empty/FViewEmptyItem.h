//
//  FViewEmptyItem.h
//  Foundation
//
//  Created by 三井 on 2019/10/15.
//

#import <Foundation/Foundation.h>
#import "FViewEmptyContainer.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ControlHandleBlock)(UIView *);
@interface FViewEmptyItem : NSObject

@property (nonatomic, assign) UIEdgeInsets insets;

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString *info;
@property (nonatomic, copy) NSString *controlTitle;
@property (nonatomic, copy) ControlHandleBlock controlEventHandler;

@property (nonatomic, strong) UIView *view;

/// 容器
@property (nonatomic, strong) FViewEmptyContainer *container;

@end

NS_ASSUME_NONNULL_END
