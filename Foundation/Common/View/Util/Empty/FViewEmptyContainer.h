//
//  FViewEmptyContainer.h
//  Foundation
//
//  Created by 三井 on 2019/10/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FViewEmptyContainer : UIControl

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString *info;
@property (nonatomic, strong) UIButton *controlButton;

@end

NS_ASSUME_NONNULL_END
