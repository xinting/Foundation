//
//  UIView+Empty.h
//  Foundation
//
//  Created by 三井 on 2019/10/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Empty)

@property (nonatomic, assign) UIEdgeInsets emptyInsets;

@property (nonatomic, strong) UIImage *emptyImage;
@property (nonatomic, copy) NSString *emptyInfo;

@property (nonatomic, strong) UIView *emptyView;

/// 如果设置了该值，在空提示出现时也会出现控制按钮，点击后回调
/// @param title 控制按钮的title
/// @param handler 回调
- (void)setControl:(NSString * _Nullable)title handler:(void (^ _Nullable)(UIView *))handler;
- (void)emptyShow:(BOOL)visible;

@end

NS_ASSUME_NONNULL_END
