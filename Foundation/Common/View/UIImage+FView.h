//
//  UIImage+FView.h
//  Foundation
//
//  Created by 新庭 on 2019/8/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (FView)

+ (UIImage *)view_imageNamed:(NSString *)name;

/// 获取图片的边界（边界外为透明）
@property (nonatomic, strong, readonly) UIBezierPath *fv_boundaryPath;

@end

NS_ASSUME_NONNULL_END
