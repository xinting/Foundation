//
//  OptionItem.h
//  Foundation
//
//  Created by 新庭 on 2019/5/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OptionItem : NSObject

@property (nonatomic, strong) NSObject *item;

+ (OptionItem *)itemWith:(NSObject *)item;

@end

NS_ASSUME_NONNULL_END
