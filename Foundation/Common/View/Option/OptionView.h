//
//  OptionView.h
//  Foundation
//
//  Created by 新庭 on 2019/5/20.
//

#import <UIKit/UIKit.h>
#import "OptionItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface OptionView : UIControl

/// 设置被选择的列表项
@property (nonatomic, copy) NSArray<OptionItem *> * items;

/**
 block格式化item为字符串，并显示在滚动栏中，同时显示在相关的TextField中
 */
@property (nonatomic, copy) NSString * (^titleFormater)(OptionItem *item);

@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, strong, readonly) OptionItem *selectedItem;

+ (instancetype)optionViewWith:(NSArray *)items;
- (NSString *)stringForItem:(OptionItem *)item;

@end

NS_ASSUME_NONNULL_END
