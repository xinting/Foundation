//
//  CircleOptionView.h
//  FFoundation
//
//  Created by 新庭 on 2020/1/5.
//

#import "OptionView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CircleOptionView : OptionView

/// 每隔step刻度显示数字，其余显示竖线. default is 10
@property (nonatomic, assign) NSUInteger step;
/// 刻度的偏移，仅使用 top
@property (nonatomic, assign) UIEdgeInsets degreeInsets;
/// 刻度显示范围, default is M_PI
@property (nonatomic, assign) CGFloat degreeAngle;

@end

NS_ASSUME_NONNULL_END
