//
//  SlideOptionCell.m
//  FFoundation
//
//  Created by 三井 on 2019/11/4.
//

#import "SlideOptionCell.h"
#import "UIView+FView.h"

@interface SlideOptionCell ()

@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@end

@implementation SlideOptionCell

- (void)refresh {
    [self updateSeparator];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self refresh];
}

- (void)updateSeparator {
    CGPathRef path = NULL;
    UIEdgeInsets insets = self.separatorItem.insets;
    switch (self.separatorItem.edge) {
        case UIRectEdgeAll:
            path = [UIBezierPath bezierPathWithRect:self.shapeLayer.bounds].CGPath;
            break;
        case UIRectEdgeRight: {
            CGMutablePathRef mPath = CGPathCreateMutable();
            CGPathMoveToPoint(mPath, NULL, self.jk_width + insets.right, insets.top);
            CGPathAddLineToPoint(mPath, NULL, self.jk_width + insets.right, self.jk_height + insets.bottom);
            path = mPath;
            break;
        }
            
        default:
            break;
    }
    self.shapeLayer.path = path;
    self.shapeLayer.strokeColor = self.separatorItem.color.CGColor;
    self.shapeLayer.lineWidth = self.separatorItem.width;
    
    switch (self.separatorItem.style) {
        case FSeparatorStyleLine:
            
            break;
            
        default:
            break;
    }
}

- (CAShapeLayer *)shapeLayer {
    if (!_shapeLayer) {
        _shapeLayer = [CAShapeLayer layer];
        _shapeLayer.fillColor = nil;
        [self.layer addSublayer:_shapeLayer];
    }
    _shapeLayer.frame = self.bounds;
    return _shapeLayer;
}

- (FSeparatorItem *)separatorItem {
    if (!_separatorItem) {
        _separatorItem = [[FSeparatorItem alloc] init];
    }
    return _separatorItem;
}

@end
