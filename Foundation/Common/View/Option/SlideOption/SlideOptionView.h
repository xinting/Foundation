//
//  SlideOptionView.h
//  Foundation
//
//  Created by 新庭 on 2019/10/20.
//

#import <UIKit/UIKit.h>
#import "OptionView.h"
#import "FSeparatorItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface SlideOptionViewSlider : UIImageView

- (void)setImage:(UIImage *)image forState:(UIControlState)state;

@end

@interface SlideOptionView : OptionView

@property (nonatomic, strong, readonly) SlideOptionViewSlider *slider;

@property (nonatomic, assign, getter=isEditing) BOOL edit;

/// Separator
@property (nonatomic, strong) FSeparatorItem *separator;

- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state;
- (UIColor *)titleColorForState:(UIControlState)state;

@end

NS_ASSUME_NONNULL_END
