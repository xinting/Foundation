//
//  SlideOptionCell.h
//  FFoundation
//
//  Created by 三井 on 2019/11/4.
//

#import <UIKit/UIKit.h>
#import "FSeparatorItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface SlideOptionCell : UIButton

@property (nonatomic, strong) FSeparatorItem *separatorItem;

@end

NS_ASSUME_NONNULL_END
