//
//  OptionItem.m
//  Foundation
//
//  Created by 新庭 on 2019/5/20.
//

#import "OptionItem.h"

@implementation OptionItem

+ (OptionItem *)itemWith:(id)item {
    OptionItem * oitem = [[OptionItem alloc] init];
    oitem.item = item;
    return oitem;
}

- (NSString *)description {
    return self.item.description;
}

- (NSString *)debugDescription {
    return self.item.debugDescription;
}

@end
