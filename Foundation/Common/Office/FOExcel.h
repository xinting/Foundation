//
//  FOExcel.h
//  AFNetworking
//
//  Created by 新庭 on 2022/3/5.
//

#import <Foundation/Foundation.h>
#import "xlsxwriter.h"

NS_ASSUME_NONNULL_BEGIN

@class FOExcelLine;
@interface FOExcel : NSObject

@property (nonatomic, strong) NSMutableArray<FOExcelLine *> *body;

- (void)save;
- (void)exportSample;

@end

@interface FOExcelContent: NSObject

@property (nonatomic, copy) NSString *text;

@end

@interface FOExcelNode: NSObject

/// 仅占位用
@property (nonatomic, assign) BOOL isBlank;
@property (nonatomic, strong) FOExcelContent *content;

+ (instancetype)nodeWithText:(NSString *)text;

- (void)writeSheet:(lxw_worksheet *)sheet inRow:(lxw_row_t)row atCol:(lxw_col_t)col;

@end

@interface FOExcelLine: NSObject

@property (nonatomic, strong) NSMutableArray<FOExcelNode *> *nodes;

+ (instancetype)lineWithTexts:(NSArray<NSString *> *)texts;

- (void)writeSheet:(lxw_worksheet *)sheet inRow:(lxw_row_t)row;

@end

NS_ASSUME_NONNULL_END
