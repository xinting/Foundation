//
//  FCache.h
//  Foundation
//
//  Created by 三井 on 2019/10/23.
//

#import <Foundation/Foundation.h>
#import "FCacheItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FCache : NSObject

@property (nonatomic, copy) NSString *key;
@property (nonatomic, strong) NSMutableArray<FCacheItem *> *items;

+ (instancetype)cacheForKey:(NSString *)key;

- (void)push:(NSObject *)obj;
- (FCacheItem *)peek;
- (FCacheItem *)pop;
- (NSArray<FCacheItem *> *)peek:(NSUInteger)num;
- (void)flush;

@end

NS_ASSUME_NONNULL_END
