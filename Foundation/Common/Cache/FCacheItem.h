//
//  FCacheItem.h
//  Foundation
//
//  Created by 三井 on 2019/10/23.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCacheItem : NSObject <YYModel, NSCoding>

@property (nonatomic, strong) NSDate *createDate;   ///< 创建时间
@property (nonatomic, strong) NSDate *visitDate;    ///< 上次访问时间
@property (nonatomic, assign) NSInteger visitTime;  ///< 被访问次数

@property (nonatomic, strong) NSObject<NSCoding> *object;

+ (instancetype)itemWithObject:(NSObject<NSCoding> *)object;

- (void)visit;

@end

NS_ASSUME_NONNULL_END
