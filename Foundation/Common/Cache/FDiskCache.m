//
//  FDiskCache.m
//  FFoundation
//
//  Created by 三井 on 2019/11/7.
//

#import "FDiskCache.h"
#import "PStorage.h"

@interface FDiskCache ()

@property (nonatomic, copy) NSString *path;

@end

@implementation FDiskCache

- (void)flush {
    if ([NSKeyedArchiver archiveRootObject:self.items toFile:self.path]) {
        
    }
}

@synthesize items = _items;
- (NSMutableArray<FCacheItem *> *)items {
    if (!_items) {
        if (self.key.length > 0) {
            NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithFile:self.path];
            if (!arr) {
                _items = [NSMutableArray array];
            } else {
                _items = [NSMutableArray arrayWithArray:arr];
            }
        }
    }
    return _items;
}

- (NSString *)path {
    if (!_path) {
        _path = [[PStorage rootForPath:@"DiskCache"] stringByAppendingPathComponent:self.key.jk_base64EncodedString];
    }
    return _path;
}

@end
