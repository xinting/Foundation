//
//  FCacheItem.m
//  Foundation
//
//  Created by 三井 on 2019/10/23.
//

#import "FCacheItem.h"

@implementation FCacheItem

+ (instancetype)itemWithObject:(NSObject<NSCoding> *)object {
    FCacheItem *item = [[FCacheItem alloc] init];
    item.object = object;
    return item;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _createDate = [NSDate date];
        _visitDate = _createDate;
        _visitTime = 1;
    }
    return self;
}

- (void)visit {
    self.visitDate = [NSDate date];
    self.visitTime++;
}

- (void)encodeWithCoder:(NSCoder *)aCoder { [self yy_modelEncodeWithCoder:aCoder]; }
- (id)initWithCoder:(NSCoder *)aDecoder { self = [super init]; return [self yy_modelInitWithCoder:aDecoder]; }
- (id)copyWithZone:(NSZone *)zone { return [self yy_modelCopy]; }
- (NSUInteger)hash { return [self yy_modelHash]; }
- (BOOL)isEqual:(id)object { return [self yy_modelIsEqual:object]; }

@end
