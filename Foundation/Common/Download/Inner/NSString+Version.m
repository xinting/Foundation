//
//  NSString+Version.m
//  DYFTManager
//
//  Created by 吴新庭 on 2019/5/16.
//

#import "NSString+Version.h"

@implementation NSString (Version)

- (NSComparisonResult)compareVersion:(NSString *)version {
    NSArray<NSString *> *me = [self componentsSeparatedByString:@"."];
    NSArray<NSString *> *u = [version componentsSeparatedByString:@"."];
    
    for (NSUInteger i = 0; i < me.count; i++) {
        if (i >= u.count) {
            // 比version大
            return NSOrderedDescending;
        }
        
        NSInteger va = me[i].integerValue;
        NSInteger vb = u[i].integerValue;
        if (va != vb) {
            return va > vb ? NSOrderedDescending : NSOrderedAscending;
        }
    }
    if (me.count < u.count) {
        // 比version小
        return NSOrderedAscending;
    }
    
    return NSOrderedSame;
}

@end
