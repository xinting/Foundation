//
//  DYDownloadSchedule.h
//  DYFTManager
//
//  Created by 吴新庭 on 2019/5/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 调度下载任务
 */
@class DYDownload;
@interface DYDownloadSchedule : NSObject

/// 最大可同时开启的任务数量，默认为5
@property (nonatomic, assign) NSUInteger max;

+ (instancetype)sharedInstance;

/**
 根据键获取下载任务

 @param key 键
 @return 下载任务
 */
- (DYDownload *)downloadForKey:(NSString *)key;
/// 清理下载任务
- (void)cleanDownload:(DYDownload *)download;

/**
 尝试开启一个下载任务。但不保证会立即开始，将被调度器收集并择机启动。

 @param download 任务实例
 */
- (void)schedule:(DYDownload *)download;
- (void)suspend:(DYDownload *)download;
- (void)resume:(DYDownload *)download;
- (void)cancel:(DYDownload *)download;

@end

NS_ASSUME_NONNULL_END
