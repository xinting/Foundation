//
//  DYDownload+Schedule.h
//  DYFTManager
//
//  Created by 吴新庭 on 2019/5/15.
//

#import "DYDownload.h"

NS_ASSUME_NONNULL_BEGIN

@interface DYDownload (Schedule)

/// 由调度器向下载任务赋值
@property (nonatomic, strong) NSURL *tmpFilePath;

- (void)setError:(NSError *)error;

@end

NS_ASSUME_NONNULL_END
