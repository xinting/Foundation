//
//  NSString+Version.h
//  DYFTManager
//
//  Created by 吴新庭 on 2019/5/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Version)

/**
 与另一个版本号比较.
 1.0 = 1.0
 1.0 > 0.9.9.9.9
 1.0.0 > 1.0
 1.0.1 > 1.0

 @param version 另一个版本号
 @return 比较的结果
 */
- (NSComparisonResult)compareVersion:(NSString *)version;

@end

NS_ASSUME_NONNULL_END
