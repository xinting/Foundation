//
//  DYDownloadVersion.h
//  DYFTManager
//
//  Created by 吴新庭 on 2019/5/16.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

NS_ASSUME_NONNULL_BEGIN

/**
 每一个键对应的一个版本记录
 */
@interface DYDownloadVersionItem : NSObject <YYModel>

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *version;
/// 文件路径，如果该路径下没有文件，将版本定为最小，防止被其它模块直接删除文件的情况发生
@property (nonatomic, copy) NSString *path;

@end

/**
 所有的版本记录
 */
@interface DYDownloadVersion : NSObject <YYModel>

@property (nonatomic, copy) NSArray<DYDownloadVersionItem *> *items;

+ (DYDownloadVersion *)version;

/**
 保存、更新版本记录
 */
- (void)save;

/**
 获取某个key对应的版本记录项

 @param key 键
 @return 版本记录
 */
- (DYDownloadVersionItem *)itemForKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
