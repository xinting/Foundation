//
//  DYDownload.h
//  DYDownload
//
//  Created by 吴新庭 on 2019/5/9.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DYDownloadConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface DYDownload : NSObject

@property (nonatomic, strong) DYDownloadConfig *config;
@property (nonatomic, assign) DYDownloadStates state;

/// 真正执行下载的任务
@property (nonatomic, strong) NSURLSessionDownloadTask *task;

/// 进度实例，可使用 UIProgressView:observedProgress = DYDownload:progress;即可实现进度条
@property (nonatomic, strong, readonly) NSProgress *progress;
@property (nonatomic, strong, readonly) NSURL *tmpFileURL;
/// 当状态为失败时将返回错误信息
@property (nonatomic, strong, readonly) NSError *error;

/**
 根据下载路径新建下载任务。此时文件下载成功后会被拷贝到Documents/DYDownload-default目录中，并以path的文件名做为文件名保存

 @param path 下载路径
 @return 下载任务
 */
+ (DYDownload *)downloadFromPath:(NSString *)path;

/**
 下载数据并保存到目标目录中

 @param path 下载路径
 @param target 目标目录
 @return 下载任务
 */
+ (DYDownload *)downloadFromPath:(NSString *)path toPath:(NSString *)target;

/**
 根据下载配置新建下载任务。下载配置中有较为细致的配置的控制

 @param conifg 配置实例
 @return 下载任务
 */
+ (DYDownload *)downloadWithConfig:(DYDownloadConfig *)conifg;

/**
 为下载实例添加事件的处理回调。
 
 如
 action:(DYDownload*)download;

 @param target 回调将被发送到指定的实例
 @param action 发送到实例的具体方法。入参为DYDownload，如果该action返回为BOOL类型，则其返回值将影响下载过程。如返回NO将阻止当前事件的继续发生
 @param downloadEvents 监听的事件集合
 
 @return 下载任务本身
 */
- (DYDownload *)addTarget:(id)target action:(nonnull SEL)action forDownloadEvents:(DYDownloadEvents)downloadEvents;

/**
 移除监听回调

 @param target 对应的target
 @param action 对应的action，如果为NULL，移除所有target对应的回调
 @param downloadEvents 对应的事件
 */
- (void)removeTarget:(id)target action:(nullable SEL)action forDownloadEvents:(DYDownloadEvents)downloadEvents;

/**
 开启下载。
 @warning 无需在子线程调用，内部下载已包含了多线程能力

 @param complete 下载完成回调。失败也会返回该回调
 */
- (void)sendComplete:(void (^_Nullable)(DYDownload *download))complete;
- (void)suspend;

/**
 将暂停的任务重启。如果任务非暂停状态，将进入排队。
 */
- (void)resume;
- (void)cancel;

@end

NS_ASSUME_NONNULL_END
