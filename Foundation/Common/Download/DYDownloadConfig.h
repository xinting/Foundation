//
//  DYDownloadConfig.h
//  DYDownload
//
//  Created by 吴新庭 on 2019/5/9.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 任务的状态，彼此互斥
typedef NS_ENUM(NSInteger, DYDownloadStates) {
    DYDownloadStateWaiting = 1,
    DYDownloadStateProcessing,
    DYDownloadStateSuspend,
    DYDownloadStateSuccess,
    DYDownloadStateFail,
    DYDownloadStateCanceld
};

/// 下载期间的事件枚举
typedef NS_OPTIONS(NSUInteger, DYDownloadEvents) {
    DYDownloadEventStart        = 1 << 0,   // 即将开始事件
    DYDownloadEventProcess      = 1 << 1,   // 正在下载期间，可用于监听进度
    DYDownloadEventSuspend      = 1 << 2,   // 任务被暂停抛出的事件
    DYDownloadEventCancel       = 1 << 3,   // 任务被取消抛出的事件
    DYDownloadEventDone         = 1 << 4,   // 下载完成后
    DYDownloadEventUnzip        = 1 << 5,   // 即将被解压
    
    DYDownloadEventAllEvents    = 0xFFFFFFFF
};

/// 下载期间失败详情
typedef NS_ENUM(NSInteger, DYDownloadError) {
    DYDownloadErrorInterrupt = 20,      // 回调返回NO，则将中断下载过程
    DYDownloadErrorVersionLower,        // 目标版本比本地版本要小
    DYDownloadErrorMD5Error,            // 文件md5比对失败
};

/// 下载任务的优先级
typedef NS_ENUM(NSInteger, DYDownloadPriority) {
    DYDownloadPriorityDefault = 0,      // 普通优先级
    DYDownloadPriorityHurry,            // 有点着急
    DYDownloadPriorityEmergency         // 非常紧急，将优先执行
};

@interface DYDownloadConfig : NSObject

/// 下载路径, required
@property (nonatomic, copy) NSString *path;

/// 以下为 optional
/// 用于索引下载任务实例
@property (nonatomic, copy) NSString *key;
/// 业务层可能会使用到的上下文
@property (nonatomic, strong) id context;

/// 目标目录
@property (nonatomic, copy) NSString *target;
/// 目标文件名
@property (nonatomic, copy) NSString *targetName;

/// 如果下载文件是zip格式，将自动解压，默认为YES
@property (nonatomic, assign) BOOL autoUnzip;

/// 下载的优先级，默认为DYDownloadPriotiryDefault
@property (nonatomic, assign) DYDownloadPriority priority;

/// 使能版本控制请配置以下值
/// 用于保存版本的key
@property (nonatomic, copy) NSString *versionKey;
/// 本次下载的目标版本。如果该目标版本小于或等于本地版本，本次下载会自动取消
@property (nonatomic, copy) NSString *versionTarget;
/// 保存于本地的该键对应的版本号
@property (nonatomic, copy, readonly) NSString *versionLocal;

/// 超时时间（s），默认为60
@property (nonatomic, assign) NSTimeInterval timeout;

/// 重试相关配置
/// 最大重试次数, 默认为 0 次，每经过一次重试，该值递减
@property (nonatomic, assign) NSInteger retryCount;
/// 重试延时，s
@property (nonatomic, assign) NSTimeInterval retryDelay;

/// 如果提供了该值，将对下载后的文件做MD5校验，校验方式为 FileHash 中的md5方法
@property (nonatomic, copy) NSString *md5;

/**
 设置日志打印的回调block。如果实例没有特殊需要，可不配置。
 实例中日志打印回调为空时，将自动回调全局日志回调中
 */
@property (nonatomic, copy) void (^logCallback)(NSString *);

/// 获取默认配置
+ (DYDownloadConfig *)defaultConfig;

/**
 获取下载配置的全局配置，该配置只有一份，每一次修改都会影响所以下载实例
 
 @return 全局配置
 */
+ (DYDownloadConfig *)globalConfig;

/**
 该方法用于方便快捷的设置配置实例中的值
 
 @param configBlock 修改该block的入参，即可修改实例本身的配置信息
 @return 返回配置实例本身
 */
- (DYDownloadConfig *)config:(void (NS_NOESCAPE ^)(DYDownloadConfig *c))configBlock;

@end

NS_ASSUME_NONNULL_END
