//
//  RPipeline+Filter.h
//  Foundation
//
//  Created by 吴新庭 on 2019/6/21.
//

#import "RPipeline.h"

NS_ASSUME_NONNULL_BEGIN

@protocol RPipelineFilterProtocol <NSObject>

@optional
/// 返回该过滤器的优先级,如果没有实现则为0
@property (nonatomic, assign, readonly) NSInteger filterPriority;

@required
/**
 如果向【流水线】添加了过滤器，当产品即将被推出时，依次经过所有的过滤器。

 @param object 流水线中的产品，或经过其它过滤器的产品
 @return 过滤后的产品，如果返回nil，该产品会被丢弃
 */
- (NSObject * _Nullable)pipelineFilter:(NSObject * _Nonnull)object;

@end

@interface RPipeline (Filter)

/**
 向【流水线】添加过滤器

 @param filter 过滤器
 */
- (void)addFilter:(id<RPipelineFilterProtocol>)filter;

@end

@interface NSObject (Filter)

/**
 在过滤器中使用。
 
 如果【产品】需要一个耗时的修改，如下载一张图片并添加到该【产品】中，而产品的推出顺序并不重要时可使用。以达到让修改完成的产品优先被推出。
 
 当该产品的耗时操作完成时，调用@code{loose}将其重新推入到流水线中，此时该产品会推入到流水线的最前端
 */
- (void)hold;
- (void)loose;

@end

NS_ASSUME_NONNULL_END
