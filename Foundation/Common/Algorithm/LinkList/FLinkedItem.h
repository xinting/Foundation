//
//  FLinkedItem.h
//  FFoundation
//
//  Created by 三井 on 2021/6/30.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// 链表的包装的数据
@interface FLinkedItem : NSObject

/// 上一数据
@property (nonatomic, strong, setter=li_setPre:) FLinkedItem * _Nullable li_pre;

/// 下一数据
@property (nonatomic, strong, setter=li_setNxt:) FLinkedItem * _Nullable li_nxt;

/// 包装数据
@property (nonatomic, strong) id data;

@end

#pragma mark -
@interface FLinkedList <__covariant ObjectType> : NSObject

/// 头节点
@property (nonatomic, strong, readonly) FLinkedItem *head;

/// 尾节点
@property (nonatomic, strong, readonly) FLinkedItem *tail;
@property (nonatomic, assign) NSUInteger length;

+ (instancetype)listWithObject:(ObjectType)object;

- (ObjectType)objectAtIndex:(NSUInteger)index;
- (void)append:(ObjectType)object;
- (void)insert:(ObjectType)object before:(NSUInteger)index;
- (ObjectType)remove:(ObjectType)object;
- (ObjectType)removeAt:(NSInteger)index;

/// 翻转
- (void)reverse;
- (void)sort:(NSComparator)compare;

@end

NS_ASSUME_NONNULL_END
