//
//  RPipeline+Input.h
//  Foundation
//
//  Created by 吴新庭 on 2019/6/21.
//

#import "RPipeline.h"

NS_ASSUME_NONNULL_BEGIN

@protocol RPipelineInputProtocol <NSObject>

@required
/**
 生产产品的方法，会由【流水线】主动调用

 @return 返回产品,如果为nil，不会被加入到流水线中
 */
- (NSObject * _Nullable)produceFor:(RPipeline *)pipeline;

@end

@interface RPipeline (Input)
 
/**
 流水线的被动生产者。如果流水线中没有产品，该方法将被主动调用
 */
@property (nonatomic, weak) id<RPipelineInputProtocol> input;

@end

NS_ASSUME_NONNULL_END
