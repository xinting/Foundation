//
//  RPipeline.h
//  Foundation
//
//  Created by 吴新庭 on 2019/6/18.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class RPipelineBatch;
/**
 流水线类
 
 适用于所有生产--消费模式的算法需要
 */
@interface RPipeline : NSObject <NSFastEnumeration>

/// 当流水线中的产品足够时，将最大推出 batchSize 数量的产品，默认为1
@property (nonatomic, assign) NSUInteger batchSize;

/// 返回一个流水线，并对每一个批次的商品执行 action，在商品使用完成后，不要忘记调用 consumed
/// 该方法已经将action放置在子线程中
/// @param action 商品操作
+ (RPipeline *)pipelineWithBlock:(void (^)(RPipelineBatch *batch))action;

/**
 当有新的【产品】时，调用该方法，推入到【流水线】中.
 
 该方法线程安全

 @param object 产品实例
 */
- (void)produce:(NSObject *)object;

/// 上一批次的商品已经消费完
- (void)consumed;

/// 销毁这个流水线
- (void)destroy;

@end

#pragma mark -
@interface RPipelineBatch : NSObject

@property (nonatomic, copy) NSArray<NSObject *> *objects;

@end

#pragma mark -
@interface NSObject (RPipeline)

/**
 当使用完当前【产品】时调用。如果该产品没有被【消费】会阻塞整个【流水线】推出新的产品
 
 该方法线程安全
 
 如果流水线中没有更多产品，将会阻塞
 
 @warning 取出产品后，该方法必须被调用
 */
- (void)consumed;

@end

NS_ASSUME_NONNULL_END
