//
//  NSDate+XCategories.m
//  FFoundation
//
//  Created by 三井 on 2020/9/24.
//

#import "NSDate+XCategories.h"

#define x_ChineseMonths @[@"正月", @"二月", @"三月", @"四月", @"五月", @"六月", @"七月", @"八月",@"九月", @"十月", @"冬月", @"腊月"]
//
////#define ChineseFestival @[@"除夕",@"春节",@"中秋",@"五一",@"国庆",@"儿童",@"圣诞",@"七夕",@"端午"]
//
#define x_ChineseDays @[@"初一", @"初二", @"初三", @"初四", @"初五", @"初六", @"初七", @"初八", @"初九", @"初十",@"十一", @"十二", @"十三", @"十四", @"十五", @"十六", @"十七", @"十八", @"十九", @"二十", @"廿一", @"廿二", @"廿三", @"廿四", @"廿五", @"廿六", @"廿七", @"廿八", @"廿九", @"三十"]
//
//#define ChineseWeatherFestival @[@"立春",@"雨水",@"惊蛰",@"春分",@"清明",@"谷雨",@"立夏",@"小满",@"忙种",@"夏至",@"小暑",@"大暑",@"立秋",@"处暑",@"寒露",@"霜降",@"白露",@"秋分",@"立冬",@"小雪",@"大雪",@"冬至",@"小寒",@"大寒"]


#define x_ChineseYears  @[@"甲子", @"乙丑", @"丙寅", @"丁卯",  @"戊辰",  @"己巳",  @"庚午",  @"辛未",  @"壬申",  @"癸酉", @"甲戌",   @"乙亥",  @"丙子",  @"丁丑", @"戊寅",   @"己卯",  @"庚辰",  @"辛己",  @"壬午",  @"癸未", @"甲申",   @"乙酉",  @"丙戌",  @"丁亥",  @"戊子",  @"己丑",  @"庚寅",  @"辛卯",  @"壬辰",  @"癸巳", @"甲午",   @"乙未",  @"丙申",  @"丁酉",  @"戊戌",  @"己亥",  @"庚子",  @"辛丑",  @"壬寅",  @"癸丑", @"甲辰",   @"乙巳",  @"丙午",  @"丁未",  @"戊申",  @"己酉",  @"庚戌",  @"辛亥",  @"壬子",  @"癸丑",  @"甲寅",   @"乙卯",  @"丙辰",  @"丁巳",  @"戊午",  @"己未",  @"庚申",  @"辛酉",  @"壬戌",  @"癸亥"]

@implementation NSDate (XCategories)

- (NSUInteger)x_lunarYear {
    return [[NSDate jk_chineseCalendar] component:NSCalendarUnitYear fromDate:self];
}

- (NSUInteger)x_lunarMonth {
    return [[NSDate jk_chineseCalendar] component:NSCalendarUnitMonth fromDate:self];
}

- (NSUInteger)x_lunarDay {
    return [[NSDate jk_chineseCalendar] component:NSCalendarUnitDay fromDate:self];
}

- (NSString *)x_lunarStringWithFormat:(NSString *)format {
    NSDateFormatter *lunarFormatter = [[NSDateFormatter alloc] init];
    lunarFormatter.dateFormat = format;
    lunarFormatter.calendar = [NSDate jk_chineseCalendar];
    
    NSString *lunar = [lunarFormatter stringFromDate:self];
    return lunar;
}

- (NSString *)x_lunarString:(NSCalendarUnit)unit {
    NSCalendar *chineseCalendar = [[self class] jk_chineseCalendar];

    NSDateComponents *localeComp = [chineseCalendar components:unit fromDate:self];
    
    NSString *y_str = unit & NSCalendarUnitYear ? [x_ChineseYears objectAtIndex:localeComp.year-1] : @"";
    NSString *m_str = unit & NSCalendarUnitMonth ? [x_ChineseMonths objectAtIndex:localeComp.month-1] : @"";
    NSString *d_str = unit & NSCalendarUnitDay ? [x_ChineseDays objectAtIndex:localeComp.day-1] : @"";
    
    NSString *chineseCal_str =[NSString stringWithFormat:@"%@%@%@",y_str,m_str,d_str];
    
    return chineseCal_str;
}

+ (NSDate *)x_dateWithLunarString:(NSString *)string format:(NSString *)format {
    NSDate *date = [NSDate jk_dateWithString:string format:format];
    
    NSDate *start = [date jk_startOfYear];
    NSCalendar *calendar = [NSDate jk_chineseCalendar];
    for (NSInteger i = 0; i < 366; i++) {
        NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:start];
        if (components.month == date.jk_month && components.day == date.jk_day) {
            return start;
        }
        start = [start jk_dateByAddingDays:1];
    }
    return nil;
}

@end
