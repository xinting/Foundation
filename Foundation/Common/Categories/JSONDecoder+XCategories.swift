//
//  JSONDecoder+XCategories.swift
//  FFoundation
//
//  Created by 三井 on 2021/5/19.
//

import Foundation

public extension JSONDecoder {
    func decode<T>(_ type: T.Type, from dic: [String: Any]) throws -> T where T : Decodable {
        if JSONSerialization.isValidJSONObject(dic as Any) {
            if let data = try? JSONSerialization.data(withJSONObject: dic as Any, options: .prettyPrinted) {
                return try JSONDecoder().decode(T.self, from: data)
            }
        }
        throw NSError(domain: "JSONDecoder+XCategories", code: 1, userInfo: [NSLocalizedDescriptionKey: "Invalide Data Type"])
    }
}
