//
//  NSArray+XCategories.h
//  Foundation
//
//  Created by 新庭 on 2019/10/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (XCategories)

/// 获取某个索引位置的值，如果索引超出，返回 @c object
/// @param index 索引位置
/// @param object 默认值
- (id)objectAtIndex:(NSUInteger)index orThen:(_Nullable id)object;

@end

NS_ASSUME_NONNULL_END
