//
//  NSDictionary+XCategories.m
//  FFoundation
//
//  Created by 新庭 on 2019/11/9.
//

#import "NSDictionary+XCategories.h"

@implementation NSDictionary (XCategories)

- (id)objectForKey:(id)aKey orThen:(id)object {
    id data = [self objectForKey:aKey];
    return data ?: object;
}

@end
