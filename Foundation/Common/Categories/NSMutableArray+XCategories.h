//
//  NSMutableArray+XCategories.h
//  Foundation
//
//  Created by 新庭 on 2019/10/24.
//

#import <Foundation/Foundation.h>
#import "NSArray+XCategories.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableArray (XCategories)

/// 获取某个索引位置的值，如果索引超出，返回 @c object; 在可变列表中将被添加，直到列表内容个数超过@c index
/// @param index 索引位置
/// @param object 默认值
- (id)objectAtIndex:(NSUInteger)index orAdd:(id)object;

@end

NS_ASSUME_NONNULL_END
