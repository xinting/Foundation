//
//  NSString+XCategories.m
//  FFoundation
//
//  Created by 新庭 on 2019/11/17.
//

#import "NSString+XCategories.h"

@implementation NSString (XCategories)

- (BOOL)isNumber {
    for (NSInteger i = 0; i < self.length; i++) {
        unichar c = [self characterAtIndex:i];
        if (c < '0' || c >'9') return NO;
    }
    return YES;
}

- (NSString *)x_htmlRemoveClass:(NSString *)clazz {
    NSString *reg = [NSString stringWithFormat:@"<\\w{1,5}? class=\"[\\w| ]*?%@.*?\".*?>.+?</a>", clazz];
    NSRange range = [self rangeOfString:reg options:NSRegularExpressionSearch];
    
    if (range.location == NSNotFound) {
        return self;
    } else {
        NSString *beRm = [self substringWithRange:range];
        return [self stringByReplacingOccurrencesOfString:beRm withString:@""];
    }
}

- (Protocol *)x_toProtocol {
    Protocol *p = NSProtocolFromString(self);
    return p;
}

- (NSString *)x_times:(NSInteger)time {
    NSMutableString *mStr = [NSMutableString stringWithString:self];
    for (NSInteger i = 1; i < time; i++) {
        [mStr appendString:self];
    }
    return mStr;
}
@end
