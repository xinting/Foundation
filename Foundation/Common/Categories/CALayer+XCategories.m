//
//  CALayer+XCategories.m
//  FFoundation
//
//  Created by 三井 on 2019/12/30.
//

#import "CALayer+XCategories.h"

@implementation CALayer (XCategories)

- (void)x_animationPause {
    CFTimeInterval time = [self convertTime:CACurrentMediaTime() fromLayer:nil];
    self.speed = 0;
    self.timeOffset = time;
}

- (void)x_animationResume {
    CFTimeInterval pause = self.timeOffset;
    
    self.speed = 1;
    self.timeOffset = 0;
    self.beginTime = 0;
    
    self.beginTime = [self convertTime:CACurrentMediaTime() fromLayer:nil] - pause;
}

@end
