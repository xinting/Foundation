//
//  NSMutableDictionary+XCategories.h
//  FFoundation
//
//  Created by 新庭 on 2019/10/30.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableDictionary (XCategories)

/// 获取某个aKey的值，如果索引超出，返回 @c object; 在可变字典中将被添加到指定key
/// @param aKey 索引key
/// @param object 默认值
- (id)objectForKey:(id)aKey orAdd:(id)object;

@end

NS_ASSUME_NONNULL_END
