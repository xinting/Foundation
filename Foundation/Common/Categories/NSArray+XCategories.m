//
//  NSArray+XCategories.m
//  Foundation
//
//  Created by 新庭 on 2019/10/24.
//

#import "NSArray+XCategories.h"

@implementation NSArray (XCategories)

- (id)objectAtIndex:(NSUInteger)index orThen:(_Nullable id)object {
    return index < self.count ? [self objectAtIndex:index] : object;
}

@end
