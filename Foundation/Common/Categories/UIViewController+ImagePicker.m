//
//  UIViewController+ImagePicker.m
//  FFoundation
//
//  Created by 三井 on 2019/11/8.
//

#import "UIViewController+ImagePicker.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "FMacroDefine.h"

F_macro_property(PickImageBlock)
@interface UIViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) void (^PickImageBlock)(id);

@end

@implementation UIViewController (ImagePicker)
F_macro_property_imp(PickImageBlock, OBJC_ASSOCIATION_COPY)

- (void)x_pickImage:(void (^)(UIImage * _Nonnull))block {
    [self x_pickImage:^(NSDictionary<UIImagePickerControllerInfoKey,id> * _Nonnull info) {
        UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
        if (!img) {
            img = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        !block ?: block(img);
    } withConfig:^(UIImagePickerController * _Nonnull picker) {
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            picker.allowsEditing = NO;
        } else if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
            picker.allowsEditing = YES;
        }
    }];
}

- (void)x_pickImage:(void (^)(NSDictionary<UIImagePickerControllerInfoKey,id> * _Nonnull))block withConfig:(void (NS_NOESCAPE ^)(UIImagePickerController * _Nonnull))config {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    self.PickImageBlock = (id)block;
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"" message:@"图片选取方式" preferredStyle:UIAlertControllerStyleActionSheet];
    
    BOOL cameraAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    NSString *takePhoto = cameraAvailable ? @"拍照" : @"拍照(当前不可用)";
    UIAlertAction *camera = [UIAlertAction actionWithTitle:takePhoto style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (!cameraAvailable) {
            return;
        }
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        !config ?: config(imagePicker);
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    camera.enabled = cameraAvailable;
    [ac addAction:camera];
    [ac addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        !config ?: config(imagePicker);
        [self presentViewController:imagePicker animated:YES completion:nil];
    }]];
    [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [self presentViewController:ac animated:YES completion:nil];
}

#pragma mark - Image Picker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    !self.PickImageBlock ?: self.PickImageBlock(info);
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
