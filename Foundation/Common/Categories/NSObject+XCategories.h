//
//  NSObject+XCategories.h
//  FFoundation
//
//  Created by 新庭 on 2019/11/9.
//

#import <Foundation/Foundation.h>

#define kxBuildObject(_clz_, _action_) [_clz_ objectWithBuilder:^(__kindof _clz_ * _Nonnull object) _action_]
NS_ASSUME_NONNULL_BEGIN

@interface NSObject (XCategories)

+ (instancetype)objectWithBuilder:(void (NS_NOESCAPE ^)(__kindof NSObject * object))build;

/// debug only
- (void)printClass;
- (void)printMethods;

@end

NS_ASSUME_NONNULL_END
