//
//  NSMutableDictionary+XCategories.m
//  FFoundation
//
//  Created by 新庭 on 2019/10/30.
//

#import "NSMutableDictionary+XCategories.h"

@implementation NSMutableDictionary (XCategories)

- (id)objectForKey:(id)aKey orAdd:(id)object {
    id obj = [self objectForKey:aKey];
    if (!obj) {
        [self setObject:object forKey:aKey];
        obj = object;
    }
    return obj;
}

@end
