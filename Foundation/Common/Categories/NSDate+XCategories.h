//
//  NSDate+XCategories.h
//  FFoundation
//
//  Created by 三井 on 2020/9/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (XCategories)

- (NSUInteger)x_lunarYear;
- (NSUInteger)x_lunarMonth;
- (NSUInteger)x_lunarDay;

- (NSString *)x_lunarStringWithFormat:(NSString *)format;
- (NSString *)x_lunarString:(NSCalendarUnit)unit;           ///< 阴历表达，如丙午八月初五
+ (NSDate *)x_dateWithLunarString:(NSString *)string format:(NSString *)format;

@end

NS_ASSUME_NONNULL_END
