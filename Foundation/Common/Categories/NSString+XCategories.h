//
//  NSString+XCategories.h
//  FFoundation
//
//  Created by 新庭 on 2019/11/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (XCategories)

@property (nonatomic, assign, readonly) BOOL isNumber;


/// 如果该字符串是html，移除指定类修饰的标签
/// @param clazz css 类
- (NSString *)x_htmlRemoveClass:(NSString *)clazz;

/// 多个相同字符串拼接成新的字符串
/// @param time 重复次数
- (NSString *)x_times:(NSInteger)time;

- (Protocol *)x_toProtocol;

@end

NS_ASSUME_NONNULL_END
