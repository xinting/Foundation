//
//  UIViewController+ImagePicker.h
//  FFoundation
//
//  Created by 三井 on 2019/11/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (ImagePicker)

/// 通过 UIImagePickerController 获取图片
/// @param block 回调
/// @param config 配置
- (void)x_pickImage:(void (^)(NSDictionary<UIImagePickerControllerInfoKey, id> *info))block withConfig:(void (NS_NOESCAPE ^)(UIImagePickerController *picker))config;
- (void)x_pickImage:(void (^)(UIImage * _Nullable img))block;

@end

NS_ASSUME_NONNULL_END
