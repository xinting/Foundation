//
//  NSMutableArray+XCategories.m
//  Foundation
//
//  Created by 新庭 on 2019/10/24.
//

#import "NSMutableArray+XCategories.h"

@implementation NSMutableArray (XCategories)

- (id)objectAtIndex:(NSUInteger)index orThen:(id)object {
    if (index >= self.count) {
        return object;
    }
    return self[index];
}

- (id)objectAtIndex:(NSUInteger)index orAdd:(id)object {
    if (index >= self.count) {
        for (NSInteger i = self.count; i <= index; i++) {
            [self addObject:object];
        }
        return object;
    }
    return self[index];
}

@end
