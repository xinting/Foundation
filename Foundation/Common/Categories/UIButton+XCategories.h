//
//  UIButton+XCategories.h
//  FFoundation
//
//  Created by 新庭 on 2020/4/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (XCategories)

- (void)x_startTime:(NSInteger )timeout title:(NSString *)tittle waitTittle:(NSString *)waitTittle;

@end

NS_ASSUME_NONNULL_END
