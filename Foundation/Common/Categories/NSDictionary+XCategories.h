//
//  NSDictionary+XCategories.h
//  FFoundation
//
//  Created by 新庭 on 2019/11/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (XCategories)

/// 获取某个aKey的值，如果索引超出，返回 @c object
/// @param aKey 索引key
/// @param object 默认值
- (id)objectForKey:(id)aKey orThen:(id)object;

@end

NS_ASSUME_NONNULL_END
