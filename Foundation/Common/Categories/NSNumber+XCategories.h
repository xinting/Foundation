//
//  NSNumber+XCategories.h
//  Foundation
//
//  Created by 吴新庭 on 2019/5/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSNumber (XCategories)

/**
 从指定起始数，并以一定跨度，生成到该数的等差数列

 @param step 跨度、数差
 @param begin 起始数
 @return 返回列表
 */
- (NSArray<NSNumber *> *)x_arrayWithStep:(NSNumber *)step from:(NSNumber *)begin;
/// 以0为起始，1为跨度生成数列
- (NSArray<NSNumber *> *)x_array;

- (void)x_step:(void (^)(NSInteger current, NSInteger total))block;

@end

NS_ASSUME_NONNULL_END
