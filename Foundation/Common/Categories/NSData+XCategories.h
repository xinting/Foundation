//
//  NSData+XCategories.h
//  FFoundation
//
//  Created by 三井 on 2019/12/31.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSData (XCategories)

- (NSData *)AES:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
