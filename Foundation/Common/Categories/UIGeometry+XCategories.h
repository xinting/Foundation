//
//  UIGeometry+XCategories.h
//  Foundation
//
//  Created by 新庭 on 2019/10/25.
//

#import <UIKit/UIGeometry.h>

NS_ASSUME_NONNULL_BEGIN

UIKIT_STATIC_INLINE CGFloat UIEdgeInsetsHeight(UIEdgeInsets insets) {
    return insets.top + insets.bottom;
}

UIKIT_STATIC_INLINE CGFloat UIEdgeInsetsWidth(UIEdgeInsets insets) {
    return insets.left + insets.right;
}

NS_ASSUME_NONNULL_END
