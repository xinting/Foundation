//
//  CALayer+XCategories.h
//  FFoundation
//
//  Created by 三井 on 2019/12/30.
//

#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface CALayer (XCategories)

- (void)x_animationPause;
- (void)x_animationResume;

@end

NS_ASSUME_NONNULL_END
