//
//  NSObject+XCategories.m
//  FFoundation
//
//  Created by 新庭 on 2019/11/9.
//

#import "NSObject+XCategories.h"
#import "NSString+XCategories.h"
#import <objc/runtime.h>

@implementation NSObject (XCategories)

+ (instancetype)objectWithBuilder:(void (NS_NOESCAPE ^)(__kindof NSObject * _Nonnull))build {
    NSObject *obj = [[[self class] alloc] init];
    !build ?: build(obj);
    return obj;
}

#if DEBUG
- (void)printClass {
    Class klass = object_getClass(self);
    NSLog(@"%@", [@"+" x_times:20]);
    NSLog(@"当前类 isa %s", object_getClassName(self));
    NSLog(@"父类 %@", NSStringFromClass(class_getSuperclass(klass)));
    NSLog(@"%@", [@"+" x_times:20]);
}

- (void)printMethods {
    unsigned int outCount = 0;
    Class klass = object_getClass(self);
    Method *methods = class_copyMethodList(klass, &outCount);
    NSLog(@"%@", [@"+" x_times:20]);
    for (int i = 0; i < outCount; i++) {
        Method method = methods[i];
        SEL name = method_getName(method);
        NSLog(@"方法 %@", NSStringFromSelector(name));
//        NSMethodSignature *sig = [NSMethodSignature signatureWithObjCTypes:method_getTypeEncoding(method)];
//        NSInvocation *invoke = [NSInvocation invocationWithMethodSignature:sig];
//        invoke.target = self;
//        invoke.selector = name;
//        [invoke invoke];
    }
    NSLog(@"%@", [@"+" x_times:20]);
}
#else
- (void)printClass {}
- (void)printMethods {}
#endif

@end
