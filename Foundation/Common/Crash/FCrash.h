//
//  FCrash.h
//  FFoundation
//
//  Created by 三井 on 2021/6/30.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCrash : NSObject

+ (void)install;

@end

NS_ASSUME_NONNULL_END
