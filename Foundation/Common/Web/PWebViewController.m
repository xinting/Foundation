//
//  PWebViewController.m
//  Foundation
//
//  Created by 吴新庭 on 2019/8/20.
//

#import "PWebViewController.h"
#import <Masonry/Masonry.h>
#import <JKCategories/JKUIKit.h>

@interface PWebViewController ()

@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *refreshButton;
@property (nonatomic, strong) UIButton *moreButton;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIView *rightView;

@end

@implementation PWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.leftView addSubview:self.backButton];
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_offset(0);
        make.leading.mas_offset(-10);
    }];
    [self.leftView addSubview:self.closeButton];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.backButton.mas_trailing).mas_offset(10);
        make.top.bottom.trailing.mas_offset(0);
    }];
    
    [self.rightView addSubview:self.moreButton];
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(0);
    }];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:self.leftView]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:self.rightView]];
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(0);
    }];
}

- (void)refresh {
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
}

#pragma mark - Action
- (void)clickBack:(UIButton *)btn {
    if (self.webView.canGoBack) {
        [self.webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)clickClose:(UIButton *)btn {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickRefresh:(UIButton *)btn {
    [self refresh];
}

- (void)clickMore:(UIButton *)btn {
    
}

#pragma mark - Private
@synthesize webView = _webView;
- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc] init];
    }
    return _webView;
}

- (UIView *)leftView {
    if (!_leftView) {
        _leftView = [[UIView alloc] init];
    }
    return _leftView;
}

- (UIView *)titleView {
    if (!_titleView) {
        _titleView = [[UIView alloc] init];
    }
    return _titleView;
}

- (UIView *)rightView {
    if (!_rightView) {
        _rightView = [[UIView alloc] init];
    }
    return _rightView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightRegular];
    }
    return _titleLabel;
}

- (UIButton *)backButton {
    if (!_backButton) {
        _backButton = [[UIButton alloc] init];
        NSString *path = [[NSBundle bundleForClass:self.class] pathForResource:@"FWeb" ofType:@"bundle"];
        NSBundle *bundle = [NSBundle bundleWithPath:path];
        [_backButton setImage:[UIImage imageNamed:@"back" inBundle:bundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
        _backButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _backButton.imageEdgeInsets = UIEdgeInsetsMake(4, 0, 4, 0);
        [_backButton setTitle:@"返回" forState:UIControlStateNormal];
        [_backButton setTitleColor:[UIColor jk_colorWithHex:0x333333] forState:UIControlStateNormal];
        _backButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_backButton addTarget:self action:@selector(clickBack:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [[UIButton alloc] init];
        [_closeButton setTitle:@"关闭" forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(clickClose:) forControlEvents:UIControlEventTouchUpInside];
        _closeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_closeButton setTitleColor:[UIColor jk_colorWithHex:0x333333] forState:UIControlStateNormal];
    }
    return _closeButton;
}

- (UIButton *)refreshButton {
    if (!_refreshButton) {
        _refreshButton = [[UIButton alloc] init];
        [_refreshButton addTarget:self action:@selector(clickRefresh:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _refreshButton;
}

- (UIButton *)moreButton {
    if (!_moreButton) {
        _moreButton = [[UIButton alloc] init];
        _moreButton.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4);
        [_moreButton addTarget:self action:@selector(clickMore:) forControlEvents:UIControlEventTouchUpInside];
        [_moreButton setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    }
    return _moreButton;
}

@end
