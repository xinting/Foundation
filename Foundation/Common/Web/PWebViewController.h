//
//  PWebViewController.h
//  Foundation
//
//  Created by 吴新庭 on 2019/8/20.
//

#import "PBaseViewController.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSInteger, PWebViewNaviType) {
    PWebViewNaviTypeDefault = 1,
    PWebViewNaviTypeBack    = PWebViewNaviTypeDefault,
    PWebViewNaviTypeClose   = 1 << 1,
    PWebViewNaviTypeMore    = 1 << 2,
    PWebViewNaviTypeRefresh = 1 << 3
};

@interface PWebViewController : PBaseViewController

@property (nonatomic, assign) PWebViewNaviType naviType;

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong, readonly) WKWebView *webView;

@end

NS_ASSUME_NONNULL_END
