//
//  FAppDelegate.h
//  FFoundation
//
//  Created by 三井 on 2019/12/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) Class naviClass;
@property (nonatomic, strong) Class rootClass;

@end

NS_ASSUME_NONNULL_END
