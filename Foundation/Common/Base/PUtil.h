//
//  PUtil.h
//  iPiano
//
//  Created by wuxinting on 16/1/14.
//  Copyright © 2016年 wuxinting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDefine.h"

@interface PUtil : NSObject

// 拼接url
+ (NSURL*)joinURL:(NSArray<NSString*>*)urls;

// URL 编码
+ (NSString*)urlEncode:(NSString*)str;

// URL 解码
+ (NSString*)urlDecode:(NSString*)str;

// 以形心为中心收缩矩形
+ (CGRect)shrinkRect:(CGRect)rect withRatio:(CGFloat)ratio;

// 获取URL中的文件名，去除后缀
+ (NSString*)pathWithoutExtension:(NSURL*)url;

@end

/**
 约束比例下的合理size

 @param size 原size
 @param ratio 约束高/宽比
 @return 返回在原size下，并约束高/宽比为ratio时，最大的size
 */
CGSize CGSizeInHWRatio(CGSize size, CGFloat ratio);

/**
 约束size的情况下，矩形的中心矩形

 @param rect 原矩形
 @param size 约束size
 @return 在指定约束的size下，位于原矩形中心的矩形
 */
CGRect CGRectCenterInSize(CGRect rect, CGSize size);
CGRect CGRectFromSize(CGSize size);
