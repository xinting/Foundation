//
//  PBaseViewController.h
//  Foundation
//
//  Created by 吴新庭 on 2019/4/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PBaseViewController : UIViewController

/**
 刷新方法，用于子类
 */
- (void)refresh;
- (void)notifyDataChanged;

/// Override
- (void)prepareSubviews;

/// Override
- (void)viewDidChangeStatusBarOrientation:(BOOL)isPortrait;

/// Keyboard
- (void)keyboardWillAppear:(NSNotification *)info;
- (void)keyboardDidAppear:(NSNotification *)info;
- (void)keyboardWillDisappear:(NSNotification *)info;
- (void)keyboardDidDisappear:(NSNotification *)info;

@end

typedef void (^PBaseViewControllerReturn)(__kindof UIViewController * vc);
@interface UIViewController (PBase)

@property (nonatomic, copy) PBaseViewControllerReturn onReturn;

@end

NS_ASSUME_NONNULL_END
