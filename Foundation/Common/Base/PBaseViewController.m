//
//  PBaseViewController.m
//  Foundation
//
//  Created by 吴新庭 on 2019/4/2.
//

#import "PBaseViewController.h"
#import "PDefine.h"
#import "FMacroDefine.h"

@interface PBaseViewController ()

@end

@implementation PBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDisappear:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)refresh {}
- (void)notifyDataChanged {}
- (void)prepareSubviews {}
- (void)viewDidChangeStatusBarOrientation:(BOOL)isPortrait {}

- (void)keyboardWillAppear:(NSNotification *)info {}
- (void)keyboardDidAppear:(NSNotification *)info {}
- (void)keyboardWillDisappear:(NSNotification *)info {}
- (void)keyboardDidDisappear:(NSNotification *)info {}

#pragma mark - Action
- (void)orientationChanged:(NSNotification *)noti {
    [self viewDidChangeStatusBarOrientation:IsPortrait];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

#pragma mark - View Controller Baes
F_macro_property(onReturn)
@implementation UIViewController (PBase)

- (PBaseViewControllerReturn)onReturn {
    return objc_getAssociatedObject(self, F_macro_property_key(onReturn));
}

- (void)setOnReturn:(PBaseViewControllerReturn)onReturn {
    objc_setAssociatedObject(self, F_macro_property_key(onReturn), onReturn, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
