//
//  PStorage.h
//  iPiano
//
//  Created by wuxinting on 16/1/22.
//  Copyright © 2016年 wuxinting. All rights reserved.
//

#import <Foundation/Foundation.h>

#define D_STORAGE_ROOT (@"piano")

@interface PStorage : NSObject

/**
 *  获取根文件目录
 *
 *  @return 返回路径
 */
+ (NSURL*)rootDir;
+ (NSString *)rootForPath:(NSString *)path;

/**
 *  判断一个文件是否存在
 *
 *  @param path 文件路径
 *
 *  @return 存在YES
 */
+ (BOOL)isExist:(NSURL*)path;
+ (NSString *)createFile:(NSString *)path;

@end
