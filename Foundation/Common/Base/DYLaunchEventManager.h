//
//  DYLaunchEventManager.h
//  DYZB
//
//  Created by 吴新庭 on 2018/7/20.
//  Copyright © 2018年 mydouyu. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ExecuteLater(action)     [[DYLaunchEventManager sharedInstance] act:^{ \
    action; \
} beforeEvent:DYLaunchEventFuture];

typedef void (^DYLaunchActionBlock)(void);

/**
 @enum DYLaunchEvent
 @brief APP启动过程的主要时刻
 
 @remark 请尽量不要在+ (void)load中有业务逻辑

 @constant DYLaunchEventLaunchBegin: APP刚启动，仅作标记，不应该有业务代码在此之前执行
 @constant DYLaunchEventCustomBegin: 业务逻辑开始执行
 @constant DYLaunchEventUIBegin: 开始初始化首页UI
 @constant DYLaunchEventLaunchEnd: 系统启动完成，对应didFinishLaunch结束
 @constant DYLaunchEventUIEnd: 首页UI didAppear (此时可能还在展示运营图和广告图)
 @constant DYLaunchEventHomeDisplayed: 首页UI真正显示 （运营图和广告图已经dismiss）
 @constant DYLaunchEventFuture: UI显示之后
 */
typedef NS_ENUM(NSInteger, DYLaunchEvent) {
    DYLaunchEventLaunchBegin = 1,
    DYLaunchEventCustomBegin,
    DYLaunchEventUIBegin,
    DYLaunchEventLaunchEnd,
    DYLaunchEventUIEnd,
    DYLaunchEventHomeDisplayed,
    DYLaunchEventFuture
};
/**
 需要在App启动时执行的业务使用
 */
@interface DYLaunchEventManager : NSObject

/// 启动广告图不再展示
@property (nonatomic, assign) BOOL launchAdDismissed;
/// 首页展示完成
@property (nonatomic, assign) BOOL launchHomeUIEnd;
/// 事件
@property (nonatomic, assign) DYLaunchEvent event;
/// 单例
+ (DYLaunchEventManager *)sharedInstance;

/**
 @brief 指定某个逻辑块在某个时刻到达之前完成。

 @param action 逻辑块
 @param event 时刻。如果event 为 DYLaunchEventFuture，那么action会在APP空闲时执行
 */
- (void)act:(DYLaunchActionBlock)action beforeEvent:(DYLaunchEvent)event;

@end

#pragma mark - Event for Object
@interface NSObject (LaunchEvent)

- (void)pushEvent:(NSObject *)event;
- (NSObject *)popEvent;

@end
