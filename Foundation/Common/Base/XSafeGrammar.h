//
//  XSafeGrammar.h
//  Course
//
//  Created by wuxinting on 2016/12/16.
//  Copyright © 2016年 xinting. All rights reserved.
//

#ifndef XSafeGrammar_h
#define XSafeGrammar_h

#import <Foundation/Foundation.h>

#define WS __weak typeof(self) wSelf = self;
#define CAT(_a, _b) ((_a)##(_b))

#define XArrayCheck( _arr_ ) ((_arr_)&&[(_arr_) isKindOfClass:[NSArray class]]&&((NSArray*)(_arr_)).count>0)
#define XSetCheck( _set_ ) ((_set_)&&[(_set_) isKindOfClass:[NSSet class]]&&((NSSet*)(_set_)).count>0)
#define XStringCheck( _str_ ) ((_str_)&&[(_str_) isKindOfClass:[NSString class]]&&((NSString*)(_str_)).length>0)
#define XClassCheck( _obj_, _clz_ ) ((_obj_)&&[_obj_ isKindOfClass:_clz_])
#define XDelegateCheck( _dlg_, _sel_ ) ((_dlg_)&&[_dlg_ respondsToSelector:@selector(_sel_)])

#define XGetMutableArray( _var_ ) ((!_var_) ? (_var_=[NSMutableArray array],_var_) : _var_)

@interface XSafeGrammar : NSObject
@end

@interface NSMutableDictionary (SafeGrammar)

- (void)safeSetObject:(id)anObject forKey:(id<NSCopying>)aKey;

@end

@interface NSMutableArray (SafeGrammar)

- (void)safeAddObject:(id)anObject;

@end

#endif /* XSafeGrammar_h */
