//
//  XSafeGrammar.m
//  Course
//
//  Created by wuxinting on 2017/10/24.
//  Copyright © 2017年 xinting. All rights reserved.
//

#import "XSafeGrammar.h"
#import <objc/runtime.h>

@implementation NSMutableDictionary (SafeGrammar)

- (void)safeSetObject:(id)anObject forKey:(id<NSCopying>)aKey
{
    if (!anObject || !aKey) return;
    
    [self setObject:anObject forKey:aKey];
}

@end

@implementation NSMutableArray (SafeGrammar)

- (void)safeAddObject:(id)anObject {
    if (!anObject) return;
    [self addObject:anObject];
}

@end

@implementation XSafeGrammar

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
    });
}

#pragma mark - Private
+ (void)swizzMethod:(SEL)aSelect toMethod:(SEL)bSelect ofClass:(Class)clz
{
    Method aMethod = class_getInstanceMethod(clz, aSelect);
    Method bMethod = class_getInstanceMethod(clz, bSelect);
    method_exchangeImplementations(aMethod, bMethod);
}

@end
