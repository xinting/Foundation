//
//  PStorage.m
//  iPiano
//
//  Created by wuxinting on 16/1/22.
//  Copyright © 2016年 wuxinting. All rights reserved.
//

#import "PStorage.h"

/**
 *  文件相关
 */
@implementation PStorage

+ (NSURL*)rootDir {
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSURL *root = [documentsDirectoryURL URLByAppendingPathComponent:D_STORAGE_ROOT];
    
    if (![self isExist:root]) {
        [[NSFileManager defaultManager] createDirectoryAtURL:root withIntermediateDirectories:YES attributes:nil error:nil];
    };
    return root;
}

+ (NSString *)rootForPath:(NSString *)path {
    NSString *fullPath = [[[self rootDir] path] stringByAppendingPathComponent:path];
    return [self createFile:fullPath];
}

+ (BOOL)isExist:(NSURL *)path {
    return [[NSFileManager defaultManager] fileExistsAtPath:path.path];
}

+ (NSString *)createFile:(NSString *)path {
    if (![self isExist:[NSURL fileURLWithPath:path]]) {
        [[NSFileManager defaultManager] createDirectoryAtURL:[NSURL fileURLWithPath:path] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return path;
}

@end
