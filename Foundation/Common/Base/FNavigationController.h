//
//  FNavigationController.h
//  Foundation
//
//  Created by 新庭 on 2019/10/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FNavigationController : UINavigationController <UIGestureRecognizerDelegate>

@end

NS_ASSUME_NONNULL_END
