//
//  FNavigationController.m
//  Foundation
//
//  Created by 新庭 on 2019/10/23.
//

#import "FNavigationController.h"

@interface FNavigationController ()

@end

@implementation FNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.interactivePopGestureRecognizer.delegate = self;
}

#pragma mark - Gesture
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return !(self.navigationController.viewControllers.count == 1);
}

@end
