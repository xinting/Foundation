//
//  PDefine.h
//  iPiano
//
//  Created by wuxinting on 16/1/13.
//  Copyright © 2016年 wuxinting. All rights reserved.
//

#ifndef PDefine_h
#define PDefine_h

// 时间格式
#define D_FORMAT_DATE @"EEE MMM d HH:mm:ss yyyy"

#pragma mark - Singleton

#define DEC_SINGLETON( _clz_ ) \
+ (_clz_ *)sharedInstance;

#define DEF_SINGLETON( _clz_ ) \
+ (_clz_ *)sharedInstance \
{\
static dispatch_once_t once;\
static _clz_ * __singleton__;\
dispatch_once( &once, ^{\
__singleton__ = [[_clz_ alloc] init];\
});\
return __singleton__;\
}

#define DAddNib( _name_ ) do{\
UINib *nib = [UINib nibWithNibName:_name_ bundle:nil];\
UIView *nibView = [nib instantiateWithOwner:self options:nil].firstObject;\
[self addSubview:nibView];\
[nibView mas_makeConstraints:^(MASConstraintMaker *make) {make.edges.mas_offset(0);}];}while(0);

#define DAddNibForSelf DAddNib(NSStringFromClass(self.class))

#define IsPortrait UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)

#pragma mark - Date

#pragma mark - UI

// rgba颜色
#define D_UI_COLOR_RGBA(_r, _g, _b, _a) [UIColor colorWithRed:(_r) green:(_g) blue:(_b) alpha:(_a)]

// 度数转角度数
#define D_UI_DEGREE_2_RADIANS(_angle) ((_angle) / 180.0 * M_PI)

// 是否需要新手引导的key
#define kFirstGuide( _clz_ ) ([NSString stringWithFormat:@"First_Guide_%@", _clz_.class])

#pragma mark -

FOUNDATION_STATIC_INLINE NSComparisonResult NSOrderReverse(NSComparisonResult order) {
    return (order == NSOrderedSame ?
            NSOrderedSame : (order == NSOrderedAscending ?
                             NSOrderedDescending :
                             NSOrderedAscending));
}

#endif /* PDefine_h */
