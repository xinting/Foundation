//
//  RWeekDefine.m
//  Roing
//
//  Created by 三井 on 2019/9/30.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWeekDefine.h"

inline NSString * PWeekDayString(PWeekDay wd)  {
    return @[@"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六"][wd];
}
