//
//  PError.h
//  iPiano
//
//  Created by wuxinting on 16/1/18.
//  Copyright © 2016年 wuxinting. All rights reserved.
//

#import <Foundation/Foundation.h>

#define D_KEY_ERROR @"error"
#define D_KEY_ERR_CODE @"code"
#define D_KEY_ERR_MSG @"msg"

#define D_ERR_CODE_SUCCESS (0)
#define D_ERR_CODE_UNKNOWN (-1)

#define D_ERR_MSG_UNKNOWN @"我也不知道发生了什么.."

// Error Code
#define D_ERR_NOT_LOGIN (4)

FOUNDATION_EXTERN NSInteger const ErrorBase;
FOUNDATION_EXTERN NSInteger const ErrorDoesNotExist;

FOUNDATION_EXTERN NSInteger const ErrorLoginBase;
FOUNDATION_EXTERN NSInteger const ErrorLoginPasswordIncorrect;
FOUNDATION_EXTERN NSInteger const ErrorLoginUserExist;
FOUNDATION_EXTERN NSInteger const ErrorLoginUserNameOrPasswordIllegal;
FOUNDATION_EXTERN NSInteger const ErrorLoginEnd;

@interface PError : NSObject

@property (nonatomic, readonly) NSInteger code;
@property (nonatomic, readonly) NSString *msg;
@property (nonatomic, strong) NSError *error;

+ (PError *)errorWithError:(NSError *)error;
+ (PError *)errorWithResponse:(NSHTTPURLResponse *)res;
+ (PError *)errorWithCode:(NSInteger)code andMsg:(NSString *)msg;

@end

@interface NSError (PError)

@property (nonatomic, copy, readonly) NSString *message;

@end
