//
//  XConfig.m
//  Course
//
//  Created by wuxinting on 2017/10/24.
//  Copyright © 2017年 xinting. All rights reserved.
//

#import "XConfig.h"

@interface XConfig ()

@property (nonatomic, strong) NSUserDefaults * config;

@end

@implementation XConfig

DEF_SINGLETON(XConfig)

- (instancetype)init
{
    if (self = [super init]) {
        self.config = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

- (BOOL)save:(id<NSCoding>)anObject forKey:(NSString *)aKey
{
    [self.config setObject:anObject forKey:aKey];
    return YES;
}

- (id)dataForKey:(NSString *)aKey
{
    return [self.config objectForKey:aKey];
}

@end
