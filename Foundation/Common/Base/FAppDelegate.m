//
//  FAppDelegate.m
//  FFoundation
//
//  Created by 三井 on 2019/12/26.
//

#import "FAppDelegate.h"
#import "FNavigationController.h"

@implementation FAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary<UIApplicationLaunchOptionsKey,id> *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    UINavigationController *navi = [[self.naviClass alloc] initWithRootViewController:[[self.rootClass alloc] init]];
    self.window.rootViewController = navi;
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (Class)naviClass {
    if (!_naviClass) {
        _naviClass = FNavigationController.class;
    }
    return _naviClass;
}

- (Class)rootClass {
    if (!_rootClass) {
        _rootClass = UIViewController.class;
    }
    return _rootClass;
}

@end
