//
//  XConfig.h
//  Course
//
//  Created by wuxinting on 2017/10/24.
//  Copyright © 2017年 xinting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDefine.h"

@interface XConfig : NSObject

DEC_SINGLETON(XConfig)

- (BOOL)save:(id<NSCoding>)anObject forKey:(NSString *)aKey;
- (id)dataForKey:(NSString *)aKey;

@end
