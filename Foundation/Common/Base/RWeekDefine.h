//
//  RWeekDefine.h
//  Roing
//
//  Created by 三井 on 2019/9/30.
//  Copyright © 2019 iyinyue. All rights reserved.
//

#ifndef RWeekDefine_h
#define RWeekDefine_h

typedef NS_ENUM(NSInteger, PWeekDay) {
    PWeekDaySun = 0,
    PWeekDayMon,
    PWeekDayTue,
    PWeekDayWed,
    PWeekDayThu,
    PWeekDayFri,
    PWeekDaySat,
    PWeekDayNum
};

/**
 获取星期字符串

 @param wd 星期枚举
 @return 字符串
 */
NSString * PWeekDayString(PWeekDay wd);

#endif /* RWeekDefine_h */
