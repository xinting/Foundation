//
//  FPDFView.h
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import <UIKit/UIKit.h>
#import "FPDFDocument.h"

NS_ASSUME_NONNULL_BEGIN

@class FPDFView;
@protocol FPDFViewDelegate <NSObject>

@optional
- (void)pdfView:(FPDFView *)pdfView didSelected:(FPDFPage *)page;
- (void)pdfView:(FPDFView *)pdfView didDisplay:(FPDFPage *)page;

@end

@interface FPDFView : UIView

@property (nonatomic, strong, readonly) UICollectionView *collectionView;

@property (nonatomic, copy, readonly) NSString *path;
@property (nonatomic, strong, readonly) FPDFDocument *document;
@property (nonatomic, strong) FPDFPage *currentPage;

@property (nonatomic, weak) id<FPDFViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame pdf:(NSString *)path;
- (void)refresh;

@end

NS_ASSUME_NONNULL_END
