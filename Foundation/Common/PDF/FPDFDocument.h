//
//  FPDFDocument.h
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import <Foundation/Foundation.h>
#import "FPDFPage.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPDFDocument : NSObject

@property (nonatomic, copy, readonly) NSString *path;
@property (nonatomic, copy, readonly) NSArray<FPDFPage *> *pages;

+ (FPDFDocument *)pdfForPath:(NSString *)path;

@end

NS_ASSUME_NONNULL_END
