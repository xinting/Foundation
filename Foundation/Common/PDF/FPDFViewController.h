//
//  FPDFViewController.h
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import "PBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class FPDFViewController, FPDFPage;
@protocol FPDFViewControllerDelegate <NSObject>

@optional
- (void)pdfViewController:(FPDFViewController *)vc displayPage:(FPDFPage *)page;
- (void)pdfViewControllerDidDisappear:(FPDFViewController *)vc;

@end

@interface FPDFViewController : PBaseViewController

@property (nonatomic, strong, readonly) NSURL *url;
@property (nonatomic, strong) FPDFPage *currentPage;
@property (nonatomic, assign) CGPoint contentOffset;

@property (nonatomic, weak) id<FPDFViewControllerDelegate> delegate;

- (instancetype)initWithURL:(NSURL *)url;

@end

NS_ASSUME_NONNULL_END
