//
//  FPDFPage.h
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FPDFPage : NSObject

@property (nonatomic, assign) NSInteger index;  ///< 第几页, 从1开始

- (instancetype)initWithPage:(CGPDFPageRef)pageref;

@end

@interface FPDFPage (Draw)

/// 每次 \c drawInRect 之后，该值可返回真实的大小
@property (nonatomic, assign, readonly) CGRect pageRect;

- (void)drawInRect:(CGRect)rect forContext:(CGContextRef)context;

@end
NS_ASSUME_NONNULL_END
