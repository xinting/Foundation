//
//  FPDFPageCell.m
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import "FPDFPageCell.h"
#import "FPDFPageView.h"

@interface FPDFPageCell ()

@property (nonatomic, strong) FPDFPageView *pageView;

@end
@implementation FPDFPageCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.pageView = [[FPDFPageView alloc] init];
        [self.contentView addSubview:self.pageView];
        [self.pageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_offset(0);
        }];
    }
    return self;
}

- (void)prepareForReuse {
    [self.pageView setNeedsDisplay];
}

- (void)setPage:(FPDFPage *)page {
    _page = page;
    
    self.pageView.page = page;
}

@end
