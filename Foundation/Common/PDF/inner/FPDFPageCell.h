//
//  FPDFPageCell.h
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import <UIKit/UIKit.h>
#import "FPDFPage.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPDFPageCell : UICollectionViewCell

@property (nonatomic, strong) FPDFPage *page;

@end

NS_ASSUME_NONNULL_END
