//
//  FPDFPageView.m
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import "FPDFPageView.h"
#import "PUtil.h"

@implementation FPDFPageView

- (void)drawRect:(CGRect)rect {
    [self.page drawInRect:rect forContext:UIGraphicsGetCurrentContext()];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (CGSizeEqualToSize(self.jk_size, self.page.pageRect.size)) {
        [self invalidateIntrinsicContentSize];
    }
}

- (CGSize)intrinsicContentSize {
    if (!CGSizeEqualToSize(CGSizeZero, self.jk_size)) {
        CGFloat ratio = self.page.pageRect.size.height / self.page.pageRect.size.width;
        return CGSizeInHWRatio(self.jk_size, ratio);
    }
    return self.page.pageRect.size;
}

@end
