//
//  FPDFView.m
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import "FPDFView.h"
#import "FPDFDocument.h"
#import "FPDFPageCell.h"
#import "PUtil.h"

@interface FPDFView () <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIPinchGestureRecognizer *pinchGR;

@property (nonatomic, strong) FPDFDocument *document;
@property (nonatomic, assign) CGFloat scale;


@end

@implementation FPDFView

- (instancetype)initWithFrame:(CGRect)frame pdf:(NSString *)path {
    if (self = [super initWithFrame:frame]) {
        _path = path;
        _document = [FPDFDocument pdfForPath:path];
        _scale = 1.f;
        [self setup];
    }
    return self;
}

- (void)refresh {
    [self.collectionView reloadData];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.currentPage.index - 1 < self.document.pages.count) {
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.currentPage.index - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
        }
    });
}

- (void)setCurrentPage:(FPDFPage *)currentPage {
    _currentPage = currentPage;
}

#pragma mark - Collection
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.document.pages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FPDFPageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(FPDFPageCell.class) forIndexPath:indexPath];
    cell.page = [self.document.pages objectAtIndex:indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    FPDFPage *page = [self.document.pages objectAtIndex:indexPath.row];
    CGSize size = CGSizeInHWRatio(self.jk_size, page.pageRect.size.height / page.pageRect.size.width);
    return CGRectApplyAffineTransform(CGRectFromSize(size), CGAffineTransformScale(CGAffineTransformIdentity, self.scale, self.scale)).size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(pdfView:didDisplay:)]) {
        [self.delegate pdfView:self didDisplay:[self.document.pages objectAtIndex:indexPath.row]];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(pdfView:didSelected:)]) {
        [self.delegate pdfView:self didSelected:[self.document.pages objectAtIndex:indexPath.row]];
    }
}

#pragma mark - Action
- (void)pinching:(UIPinchGestureRecognizer *)gr {
    if (gr.state == UIGestureRecognizerStateEnded) {
        self.scale = gr.scale;
        [self.collectionView reloadData];
    }
}

#pragma mark - Private
- (void)setup {
    self.backgroundColor = [UIColor colorWithWhite:0.7 alpha:1.0];
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(0);
    }];
    
    self.pinchGR = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinching:)];
    [self addGestureRecognizer:self.pinchGR];
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        flow.minimumLineSpacing = 0;
        flow.minimumInteritemSpacing = 0;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.contentInset = UIEdgeInsetsZero;
        [_collectionView registerClass:FPDFPageCell.class forCellWithReuseIdentifier:NSStringFromClass(FPDFPageCell.class)];
    }
    return _collectionView;
}

@end
