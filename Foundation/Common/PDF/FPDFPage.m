//
//  FPDFPage.m
//  Foundation
//
//  Created by 吴新庭 on 2019/8/29.
//

#import "FPDFPage.h"

@interface FPDFPage ()

@property (nonatomic, assign) CGPDFPageRef page;
@property (nonatomic, assign) CGRect pageRect;

@end

@implementation FPDFPage

- (instancetype)initWithPage:(CGPDFPageRef)pageref {
    if (self = [super init]) {
        _page = pageref;
        
        _pageRect = CGPDFPageGetBoxRect(pageref, kCGPDFMediaBox);
    }
    return self;
}

- (void)dealloc {
}

#pragma mark - Draw
- (void)drawInRect:(CGRect)rect forContext:(nonnull CGContextRef)context {
    CGContextSaveGState(context);
    
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -rect.size.height);
    
    CGAffineTransform transform = CGPDFPageGetDrawingTransform(self.page, kCGPDFMediaBox, rect, 0, true);
    CGContextConcatCTM(context, transform);
    CGContextDrawPDFPage (context, self.page);
    
    CGContextRestoreGState(context);
}

@end
