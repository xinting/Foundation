//
//  FPlugin.h
//  Foundation
//
//  Created by 新庭 on 2019/7/7.
//

#import <UIKit/UIKit.h>

@class FPluginContext;
@interface FPluginArg : NSObject <NSCopying>

/**
 主件自身的标识符
 */
@property (nonatomic, copy) NSString *hostIdentifier;

/// 可用于跳转的导航器
@property (nonatomic, weak) UINavigationController *navi;

/**
 如果主件是列表，该属性将会有值. 可集合化的视图或实例
 */
@property (nonatomic, strong) __kindof NSObject *collectable;
@property (nonatomic, strong) FPluginContext *context;

/**
 创建一个插件入参

 @param hostIdentifier 主件id
 @return 实例
 */
+ (FPluginArg *)argWithHost:(NSString *)hostIdentifier;
+ (FPluginArg *)argWithBuilder:(void (NS_NOESCAPE ^)(FPluginArg *arg))build;

@end

@protocol FPlugin <NSObject>

@required
/// 插件自身的标识符
@property (nonatomic, copy, class, readonly) NSString *identifier;
/// 所有插入的主件标识符
@property (nonatomic, copy, class, readonly) NSArray<NSString *> *hostIdentifiers;

/**
 当主件启动时调用

 @param arg 证件的入口参数
 @return 插件启动的结果，如果为NO，将plg_unload该插件
 */
- (BOOL)plg_load:(FPluginArg *)arg;

/**
 当主件上下文有变化时调用

 @param arg 主件此时的上下文, 如果为空，表示非上下文元素有变化
 */
- (void)plg_refresh:(FPluginArg *)arg;

/**
 主件解析
 */
- (void)plg_unLoad;

@optional
/// 插件load顺序为优先级升序
@property (nonatomic, assign) NSInteger priority;

/// 获取对应插件实例
/// @param host 主件ID
+ (instancetype)instanceForHost:(NSString *)host;

@end

#pragma mark -
@interface FPluginArg (UICollectionView)

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@interface FPluginArg (UITableView)

@property (nonatomic, strong) UITableView *tableView;

@end

@interface FPluginArg (UITabBarController)

@property (nonatomic, strong) UITabBarController *tabBarController;

@end
