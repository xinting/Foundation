//
//  FPluginContext.h
//  Foundation
//
//  Created by 三井 on 2019/10/22.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, FPluginContextRefreshScope) {
    FPluginContextRefreshScopeMain      = 0x0,          ///< 主数据内容更新，默认
    FPluginContextRefreshScopeAll       = ~0UL
};

NS_ASSUME_NONNULL_BEGIN
/**
 * 主件的上下文，
 */
@interface FPluginContext : NSObject

@property (nonatomic, assign) FPluginContextRefreshScope refreshScope;

@end

NS_ASSUME_NONNULL_END
