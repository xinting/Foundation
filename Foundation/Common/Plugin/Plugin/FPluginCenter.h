//
//  FPluginCenter.h
//  Foundation
//
//  Created by 三井 on 2019/10/18.
//

#import <Foundation/Foundation.h>
#import "FPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@class FPluginStore;
@protocol FPluginCenterDelegate <NSObject>

@optional

/// 是否为指定主件加载插件，如果返回NO，将不加载该id插件
/// @param identifier 主件id
/// @param hostIdentifier 被加载插件id
- (BOOL)shouldLoad:(NSString *)identifier forHost:(NSString *)hostIdentifier;

@end
/// 管理所有相关，不相关的插件声明
@interface FPluginCenter : NSObject

@property (nonatomic, weak) id<FPluginCenterDelegate> delegate;
@property (nonatomic, strong, readonly) FPluginStore *pluginStore;

/// 插件中心为单例
+ (instancetype)sharedInstance;

/// 注册插件类，注册时并不会初始化
/// @param pluginClass 插件类
- (void)registerPlugin:(Class<FPlugin>)pluginClass;

/// 加载所有与 \c hostIdentifier 相关的插件，插件将被初始化
/// @param hostIdentifier 主件的id
/// @return 所有相关插件的实例数组
- (NSArray<id<FPlugin>> *)loadPluginFor:(NSString *)hostIdentifier;

@end

NS_ASSUME_NONNULL_END
