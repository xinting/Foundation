//
//  FPlugin.m
//  Foundation
//
//  Created by 新庭 on 2019/7/7.
//

#import "FPlugin.h"
#import <objc/runtime.h>

@implementation FPluginArg

+ (FPluginArg *)argWithHost:(NSString *)hostIdentifier {
    FPluginArg *arg = [[FPluginArg alloc] init];
    arg.hostIdentifier = hostIdentifier;
    return arg;
}

+ (FPluginArg *)argWithBuilder:(void (NS_NOESCAPE ^)(FPluginArg *))build {
    FPluginArg *arg = [[FPluginArg alloc] init];
    build(arg);
    
    NSAssert(arg.hostIdentifier.length > 0, @"主件id必须有值");
    return arg;
}

- (id)copyWithZone:(NSZone *)zone {
    FPluginArg *arg = [[FPluginArg allocWithZone:zone] init];
    arg.hostIdentifier = [self.hostIdentifier copyWithZone:zone];
    arg.navi = self.navi;
    // context 只有一份
    arg.context = self.context;
    return arg;
}

@end

#pragma mark -

@implementation FPluginArg (UICollectionView)

- (void)setCollectionView:(UICollectionView *)collectionView {
    self.collectable = collectionView;
}

- (UICollectionView *)collectionView {
    return self.collectable;
}

@end

@implementation FPluginArg (UITableView)

- (UITableView *)tableView {
    return self.collectable;
}

- (void)setTableView:(UITableView *)tableView {
    self.collectable = tableView;
}

@end

@implementation FPluginArg (UITabBarController)

- (UITabBarController *)tabBarController {
    return self.collectable;
}

- (void)setTabBarController:(UITabBarController *)tabBarController {
    self.collectable = tabBarController;
}

@end
