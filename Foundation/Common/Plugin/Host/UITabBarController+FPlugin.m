//
//  UITabBarController+FPlugin.m
//  FFoundation
//
//  Created by 三井 on 2020/1/9.
//

#import "UITabBarController+FPlugin.h"
#import "FPluginHostable.h"
#import "FPluginHostTabBarController.h"

@interface UITabBarController () <FPluginHostable>

@end

@implementation UITabBarController (FPlugin)

- (FPluginHost *)hostFor:(FPluginArg *)arg {
    FPluginHostTabBarController *host = [[FPluginHostTabBarController alloc] initWithHost:arg.hostIdentifier];
    host.tabBarController = self;
    return host;
}

@end
