//
//  UITableView+FPlugin.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import <UIKit/UIKit.h>
#import "NSObject+FPlugin.h"

FOUNDATION_EXTERN NSInteger FPluginSectionSingle;
FOUNDATION_EXTERN NSInteger FPluginSectionNatual;
FOUNDATION_EXTERN NSInteger FPluginSectionEntire;
FOUNDATION_EXTERN NSInteger FPluginNumberOfItemsInSection;

NS_ASSUME_NONNULL_BEGIN

@interface UITableView (FPlugin)

- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
