//
//  FPluginHostable.h
//  FFoundation
//
//  Created by 三井 on 2020/1/9.
//

#ifndef FPluginHostable_h
#define FPluginHostable_h

@class FPluginHost, FPluginArg, FPluginHostProxy, FPluginFake;
@protocol FPluginHostable <NSObject>

/// 通过参数创建 @c pluginHost ，需要子类重写
/// @param arg 插件入参
- (FPluginHost *)hostFor:(FPluginArg *)arg;

/// 通过参数创建 @c hostProxy
/// @param arg 插件入参
- (FPluginHostProxy *)proxyFor:(FPluginArg *)arg;

/// 由proxy返回fake
/// @param identifier 主件id
- (FPluginFake *)fakeForIdentifier:(NSString *)identifier;

@end

#endif /* FPluginHostable_h */
