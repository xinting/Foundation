//
//  UIPageViewController+FPlugin.m
//  FFoundation
//
//  Created by 三井 on 2020/10/13.
//

#import "UIPageViewController+FPlugin.h"
#import "FPluginHostPageViewController.h"
#import "FPluginProxyPageViewController.h"
#import "FPluginFakePageViewController.h"

@implementation UIPageViewController (FPlugin)

- (FPluginFake *)fakeForIdentifier:(NSString *)identifier {
    FPluginFakePageViewController *fake = [FPluginFakePageViewController fakeFor:self];
    return fake;
}

- (FPluginHost *)hostFor:(FPluginArg *)arg {
    FPluginHostPageViewController *host = [[FPluginHostPageViewController alloc] initWithHost:arg.hostIdentifier];
    arg.pageViewController = self;
    return host;
}

- (FPluginHostProxy *)proxyFor:(FPluginArg *)arg {
    FPluginProxyPageViewController *proxy = [[FPluginProxyPageViewController alloc] initWithSource:self];
    return proxy;
}

@end

@implementation FPluginArg (UIPageViewController)

- (void)setPageViewController:(UIPageViewController *)pageViewController {
    self.collectable = pageViewController;
}

- (UIPageViewController *)pageViewController {
    if ([self.collectable isKindOfClass:UIPageViewController.class]) {
        return (UIPageViewController *)self.collectable;
    }
    return nil;
}

@end
