//
//  UIPageViewController+FPlugin.h
//  FFoundation
//
//  Created by 三井 on 2020/10/13.
//

#import <UIKit/UIKit.h>
#import "FPluginHostable.h"
#import "FPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIPageViewController (FPlugin) <FPluginHostable>

@end

@interface FPluginArg (UIPageViewController)

@property (nonatomic, strong) UIPageViewController *pageViewController;

@end

NS_ASSUME_NONNULL_END
