//
//  NSObject+FPlugin.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import <UIKit/UIKit.h>
#import "FPluginHostable.h"

NS_ASSUME_NONNULL_BEGIN

@class FPluginHost, FPluginArg, FPluginContext;
@interface NSObject (FPlugin) <FPluginHostable>

/// 列表的数据源，如果需要插件化业务，请使用统一数据源
@property (nonatomic, strong, readonly) NSMutableArray *plg_datas;
@property (nonatomic, strong, readonly) FPluginHost *pluginHost;
@property (nonatomic, strong, readonly) FPluginContext *context;

/// 业务插件被的顺序，输入插件id的数组。
@property (nonatomic, copy) NSArray<NSString *> *pluginPriority;

/// 修改数据时，请务必调用以下方法包裹
- (BOOL)plg_beginUpdate;
- (void)plg_endUpdate;

/**
 为视图主件开启加载插件

 @param arg 上下文
 @return 加载的插件个数
 */
- (NSInteger)plghost_load:(FPluginArg *_Nullable)arg;

/**
 为视图主件更新插件

 @param arg 更新的上下文
 */
- (void)plghost_refresh:(FPluginArg *_Nullable)arg;

/**
 析构插件
 */
- (void)plghost_unload;

@end

NS_ASSUME_NONNULL_END
