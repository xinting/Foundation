//
//  UICollectionView+FPlugin.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "UICollectionView+FPlugin.h"
#import "FPluginCollectionViewProxy.h"
#import "FPluginCollectionViewHost.h"
#import "NSMutableArray+FPlugin.h"

@interface UICollectionView ()

@property (nonatomic, strong, readonly) FPluginCollectionViewProxy *hostProxy;

@end

@implementation UICollectionView (FPlugin)

- (id)objectAtIndexPath:(NSIndexPath *)indexPath {
    return [self.plg_datas objectAtIndexPath:indexPath withSectionType:self.hostProxy.sectionType];
}

- (void)setPluginPriority:(NSArray<NSString *> *)pluginPriority {
    [super setPluginPriority:pluginPriority];
}

- (FPluginHost *)hostFor:(FPluginArg *)arg {
    FPluginCollectionViewHost *host = [[FPluginCollectionViewHost alloc] initWithHost:arg.hostIdentifier];
    host.collectionView = self;
    return host;
}

- (FPluginCollectionViewProxy *)hostProxy {
    return (FPluginCollectionViewProxy *)self.pluginHost.hostProxy;
}

@end

NSInteger FPluginSectionSingle = 0;
NSInteger FPluginSectionNatual = 1;
NSInteger FPluginSectionEntire = NSIntegerMax;
NSInteger FPluginNumberOfItemsInSection = 1;
