//
//  UITableView+FPlugin.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "UITableView+FPlugin.h"
#import "FPluginTableViewHost.h"
#import "FPluginTableViewProxy.h"

@interface UITableView ()

@property (nonatomic, strong, readonly) FPluginTableViewProxy *hostProxy;

@end

@implementation UITableView (FPlugin)

- (id)objectAtIndexPath:(NSIndexPath *)indexPath {
    return [self.plg_datas objectAtIndexPath:indexPath withSectionType:self.hostProxy.sectionType];
}

- (FPluginHost *)hostFor:(FPluginArg *)arg {
    FPluginTableViewHost *tvHost = [[FPluginTableViewHost alloc] initWithHost:arg.hostIdentifier];
    tvHost.tableView = self;
    return tvHost;
}

- (FPluginTableViewProxy *)hostProxy {
    return (FPluginTableViewProxy *)self.pluginHost.hostProxy;
}

@end
