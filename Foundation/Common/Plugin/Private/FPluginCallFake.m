//
//  FPluginCallFake.m
//  FFoundation
//
//  Created by 三井 on 2020/9/25.
//

#import "FPluginCallFake.h"

@implementation FPluginCallFake

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel {
    if ([(NSObject *)self.source respondsToSelector:sel]) {
        return [self.source methodSignatureForSelector:sel];
    } else {
        return [NSNull methodSignatureForSelector:@selector(null)];
    }
}

- (void)forwardInvocation:(NSInvocation *)invocation {
    [invocation invokeWithTarget:self.source];
}

@end
