//
//  FPluginTableViewHost.m
//  Foundation
//
//  Created by 三井 on 2019/10/18.
//

#import "FPluginTableViewHost.h"
#import "FPluginTableViewProxy.h"

@implementation FPluginTableViewHost

- (NSUInteger)loadPlugins:(FPluginArg *)arg {
    self.hostProxy = [[FPluginTableViewProxy alloc] initWithSource:self.tableView];
    return [super loadPlugins:arg];
}

@end
