//
//  FPluginTableViewHost.h
//  Foundation
//
//  Created by 三井 on 2019/10/18.
//

#import "FPluginHost.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginTableViewHost : FPluginHost

@property (nonatomic, weak) UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
