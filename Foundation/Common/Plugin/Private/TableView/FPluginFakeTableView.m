//
//  FPluginFakeTableView.m
//  Foundation
//
//  Created by 吴新庭 on 2019/7/15.
//

#import "FPluginFakeTableView.h"
#import "FPlugin.h"
#import "FPluginWrappedData.h"
#import "UITableView+FPlugin.h"

@interface FPluginFakeTableView ()

@property (nonatomic, readonly) UITableView *sourceTable;

@end

@implementation FPluginFakeTableView

- (void)registerClass:(Class)cellClass forCellReuseIdentifier:(NSString *)identifier {
    Class<FPlugin> clz = [self.plugin class];
    NSString *plgid = [NSString stringWithFormat:@"%@-%@", [clz identifier], identifier];
    [self.source registerClass:cellClass forCellReuseIdentifier:plgid];
}

- (UITableViewCell *)dequeueReusableCellWithIdentifier:(NSString *)identifier {
    Class<FPlugin> clz = [self.plugin class];
    NSString *plgid = [NSString stringWithFormat:@"%@-%@", [clz identifier], identifier];
    return [self.source dequeueReusableCellWithIdentifier:plgid];
}

- (UITableViewCell *)dequeueReusableCellWithIdentifier:(NSString *)identifier forIndexPath:(NSIndexPath *)indexPath {
    Class<FPlugin> clz = [self.plugin class];
    NSString *plgid = [NSString stringWithFormat:@"%@-%@", [clz identifier], identifier];
    return [self.source dequeueReusableCellWithIdentifier:plgid forIndexPath:indexPath];
}

#pragma mark - Private
- (UITableView *)sourceTable {
    return (UITableView *)self.source;
}

@end
