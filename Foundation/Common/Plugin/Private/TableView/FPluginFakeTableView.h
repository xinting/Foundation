//
//  FPluginFakeTableView.h
//  Foundation
//
//  Created by 吴新庭 on 2019/7/15.
//

#import <UIKit/UIKit.h>
#import "FPluginFakeView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginFakeTableView : FPluginFakeView

@property (nonatomic, weak) id<UITableViewDelegate> delegate;
@property (nonatomic, weak) id<UITableViewDataSource> dataSource;

@end

NS_ASSUME_NONNULL_END
