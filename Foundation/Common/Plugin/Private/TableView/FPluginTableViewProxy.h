//
//  FPluginTableViewProxy.h
//  Foundation
//
//  Created by 吴新庭 on 2019/7/15.
//

#import <UIKit/UIKit.h>
#import "FPluginHostProxy.h"
#import "NSMutableArray+FPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@class FPluginFakeTableView;
@interface FPluginTableViewProxy : FPluginHostProxy

@property (nonatomic, assign) FPluginArraySectionType sectionType;

@end

NS_ASSUME_NONNULL_END
