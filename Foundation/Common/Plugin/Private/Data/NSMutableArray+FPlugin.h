//
//  NSMutableArray+FPlugin.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * 分组的类型
 */
typedef NS_ENUM(NSInteger, FPluginArraySectionType) {
    FPluginArraySectionTypeSingle,          ///< 单个分组
    FPluginArraySectionTypeNatural,         ///< 自然分组，连续的插件数据为一组.
    FPluginArraySectionTypeEntire           ///< 完全分组，每个数据单独成组. 当数据为列表时，将列表内的数据索引作为indexPath.item
};

@interface NSMutableArray (FPlugin)

/// 自然组的个数
@property (nonatomic, assign, readonly) NSInteger sectionNum;

/// 通过indexPath获取数据。
/// @param indexPath 索引
/// @param type 分组类型
- (id)objectAtIndexPath:(NSIndexPath *)indexPath withSectionType:(FPluginArraySectionType)type;
- (NSInteger)numberOfItemsInSection:(NSInteger)section withSectionType:(FPluginArraySectionType)type;;

@end

NS_ASSUME_NONNULL_END
