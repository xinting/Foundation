//
//  FPluginWrappedData.h
//  Foundation
//
//  Created by 三井 on 2019/10/18.
//

#import <Foundation/Foundation.h>
#import "FPluginFake.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginWrappedData : FPluginFake

/// 属于哪个插件
@property (nonatomic, copy) NSString *from;

/// 真实数据
@property (nonatomic, strong) id object;

+ (instancetype)wrapObject:(id)object for:(id<FPlugin>)plugin;

@end

@interface NSObject (FPluginWrappedData)

/// 属于哪个插件
@property (nonatomic, copy) NSString *from;

/// 真实数据
@property (nonatomic, strong) id object;

@end

NS_ASSUME_NONNULL_END
