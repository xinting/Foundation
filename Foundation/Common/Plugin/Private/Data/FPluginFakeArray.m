//
//  FPluginFakeArray.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "FPluginFakeArray.h"
#import "FPluginWrappedData.h"

@interface FPluginFakeArray ()

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation FPluginFakeArray

+ (instancetype)fakeFor:(id)source {
    FPluginFakeArray *fakeArr = [super fakeFor:source];
    fakeArr.array = source;
    return fakeArr;
}

- (BOOL)containsObject:(id)object {
    for (FPluginWrappedData *data in self.array) {
        if ([data.object isEqual:object]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Override
- (NSString *)description {
    return self.array.description;
}

- (NSString *)debugDescription {
    return self.array.debugDescription;
}

@end
