//
//  NSObject+FPluginData.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import <Foundation/Foundation.h>
#import "FPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (FPluginData)

@property (nonatomic, weak, readonly) id<FPlugin> plugin;

- (BOOL)isEqualPlugin:(id<FPlugin>)anPlugin;

@end

NS_ASSUME_NONNULL_END
