//
//  NSObject+FPluginData.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "NSObject+FPluginData.h"
#import "FPlugin.h"

@implementation NSObject (FPluginData)

- (BOOL)isEqualPlugin:(id<FPlugin>)anPlugin {
    if (![self conformsToProtocol:@protocol(FPlugin)]) {
        return NO;
    }
    return [[self.class identifier] isEqualToString:[anPlugin.class identifier]];
}

- (id<FPlugin>)plugin {
    return nil;
}

@end
