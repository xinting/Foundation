//
//  FPluginWrappedData.m
//  Foundation
//
//  Created by 三井 on 2019/10/18.
//

#import "FPluginWrappedData.h"

@implementation FPluginWrappedData

+ (instancetype)wrapObject:(id)object for:(nonnull id<FPlugin>)plugin {
    FPluginWrappedData *data = [FPluginWrappedData fakeFor:object];
    data.object = object;
    data.plugin = plugin;
    return data;
}

- (BOOL)isKindOfClass:(Class)aClass {
    if ([NSStringFromClass(self.class) isEqualToString:NSStringFromClass(aClass)]) {
        return YES;
    }
    return [super isKindOfClass:aClass];
}

- (NSString *)from {
    return [self.plugin.class identifier];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Wrapped: %@", [self.object description]];
}

- (NSString *)debugDescription {
    return [NSString stringWithFormat:@"Wrapped: %@", [self.object debugDescription]];
}

@end

@implementation NSObject (FPluginWrappedData)

- (NSString *)from {
    return nil;
}

- (void)setFrom:(NSString *)from {}

- (id)object {
    return self;
}

- (void)setObject:(id)object {}

@end
