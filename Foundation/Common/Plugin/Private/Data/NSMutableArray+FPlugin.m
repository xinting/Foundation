//
//  NSMutableArray+FPlugin.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "NSMutableArray+FPlugin.h"
#import "FPluginWrappedData.h"
#import "NSObject+FPluginData.h"

@implementation NSMutableArray (FPlugin)

- (NSInteger)sectionNum {
    NSInteger section = 0;
    for (NSInteger pre = 0, cur = 1; cur < self.count; pre++, cur++) {
        FPluginWrappedData *predata = self[pre];
        FPluginWrappedData *curdata = self[cur];
        if ((!predata.plugin && !curdata.plugin) || [((NSObject<FPlugin> *)predata.plugin) isEqualPlugin:curdata.plugin]) {
            
        } else {
            section++;
        }
    }
    return section + 1; // count
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath withSectionType:(FPluginArraySectionType)type {

    switch (type) {
        case FPluginArraySectionTypeSingle:
            if (self.count > indexPath.item) {
                return self[indexPath.item];
            }
            break;
            
        case FPluginArraySectionTypeNatural:
            return [self objectAtIndexPathForNatural:indexPath];
            
        default: {
            if (self.count > indexPath.section) {
                id data = self[indexPath.section];
                if ([data isKindOfClass:NSArray.class] && [data count] > indexPath.item) {
                    data = ((NSArray *)data)[indexPath.item];
                }
                return data;
            }
        }
    }
    return nil;
}

- (NSInteger)numberOfItemsInSection:(NSInteger)section withSectionType:(FPluginArraySectionType)type {
    if (self.count < 1) {
        return 0;
    }
    
    switch (type) {
        case FPluginArraySectionTypeSingle:
            return self.count;
        
        case FPluginArraySectionTypeNatural:
            return [self numberOfItemsInSectionForNatural:section];
            
        default: {
            NSInteger num = 1;
            id data = self[section];
            if ([data isKindOfClass:NSArray.class]) {
                num = [(NSArray *)data count];
            }
            return num;
        }
    }
}

#pragma mark - Private
- (id)objectAtIndexPathForNatural:(NSIndexPath *)indexPath {
    NSInteger section = 0;
    NSInteger item = 0;
    if (indexPath.section == section && indexPath.item == item) {
        return self.firstObject;
    }
    
    for (NSInteger pre = 0, cur = 1; cur < self.count; pre++, cur++) {
        FPluginWrappedData *predata = self[pre];
        FPluginWrappedData *curdata = self[cur];
        if ((!predata.plugin && !curdata.plugin) || [((NSObject<FPlugin> *)predata.plugin) isEqualPlugin:curdata.plugin]) {
            item++;
        } else {
            section++;
            item = 0;
        }
        if (indexPath.section == section && indexPath.item == item) {
            return curdata;
        }
    }
    
    return nil;
}

- (NSInteger)numberOfItemsInSectionForNatural:(NSInteger)sec {
    NSInteger section = 0;
    NSInteger item = 0;
    
    for (NSInteger pre = 0, cur = 1; cur < self.count; pre++, cur++) {
        FPluginWrappedData *predata = self[pre];
        FPluginWrappedData *curdata = self[cur];
        if ((!predata.plugin && !curdata.plugin) || [((NSObject<FPlugin> *)predata.plugin) isEqualPlugin:curdata.plugin]) {
            item++;
        } else {
            if (section == sec) {
                break;
            }
            
            section++;
            item = 0;
        }
    }
    
    return item + 1;
}

@end
