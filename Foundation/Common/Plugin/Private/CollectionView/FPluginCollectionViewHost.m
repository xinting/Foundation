//
//  FPluginCollectionViewHost.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "FPluginCollectionViewHost.h"
#import "FPluginCollectionViewProxy.h"

@implementation FPluginCollectionViewHost

- (NSUInteger)loadPlugins:(FPluginArg *)arg {
    self.hostProxy = [[FPluginCollectionViewProxy alloc] initWithSource:self.collectionView];
    return [super loadPlugins:arg];
}

@end
