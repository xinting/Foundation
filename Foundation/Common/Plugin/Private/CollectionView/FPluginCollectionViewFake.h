//
//  FPluginCollectionViewFake.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "FPluginFakeView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginCollectionViewFake : FPluginFakeView

@property (nonatomic, weak) id<UICollectionViewDelegate> delegate;
@property (nonatomic, weak) id<UICollectionViewDataSource> dataSource;

@end

NS_ASSUME_NONNULL_END
