//
//  FPluginCollectionViewFake.m
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "FPluginCollectionViewFake.h"
#import "FPluginWrappedData.h"
#import "UICollectionView+FPlugin.h"
#import "NSMutableArray+FPlugin.h"
#import "NSObject+FPluginData.h"
#import "FPluginCollectionViewHost.h"
#import "FPluginCollectionViewProxy.h"

@interface FPluginCollectionViewFake ()

@property (nonatomic, weak, readonly) UICollectionView *collectionView;

@end

@implementation FPluginCollectionViewFake

- (id)objectAtIndexPath:(NSIndexPath *)indexPath {
    FPluginWrappedData *data = [self.collectionView objectAtIndexPath:indexPath];
    return data.object;
}

- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier {
    NSString *prefixIdentifier = [NSString stringWithFormat:@"%@-%@", [self.plugin.class identifier], identifier];
    [self.collectionView registerClass:cellClass forCellWithReuseIdentifier:prefixIdentifier];
}

- (UICollectionViewCell *)dequeueReusableCellWithReuseIdentifier:(NSString *)identifier forIndexPath:(NSIndexPath *)indexPath {
    NSString *prefixIdentifier = [NSString stringWithFormat:@"%@-%@", [self.plugin.class identifier], identifier];
    return [self.collectionView dequeueReusableCellWithReuseIdentifier:prefixIdentifier forIndexPath:indexPath];
}

- (UICollectionView *)collectionView {
    return (UICollectionView *)self.source;
}

#pragma mark - Cascade Plugin
- (FPluginHost *)hostFor:(FPluginArg *)arg {
    FPluginCollectionViewHost *host = [[FPluginCollectionViewHost alloc] initWithHost:arg.hostIdentifier];
    host.collectionView = (UICollectionView *)self;
    return host;
}

- (FPluginCollectionViewProxy *)hostProxy {
    return (FPluginCollectionViewProxy *)self.pluginHost.hostProxy;
}

@end
