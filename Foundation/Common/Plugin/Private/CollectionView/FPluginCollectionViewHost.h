//
//  FPluginCollectionViewHost.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "FPluginHost.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginCollectionViewHost : FPluginHost

@property (nonatomic, weak) UICollectionView *collectionView;

@end

NS_ASSUME_NONNULL_END
