//
//  FPluginCollectionViewProxy.h
//  Foundation
//
//  Created by 三井 on 2019/10/21.
//

#import "FPluginHostProxy.h"
#import "NSMutableArray+FPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginCollectionViewProxy : FPluginHostProxy

/// 分组类型
@property (nonatomic, assign) FPluginArraySectionType sectionType;

@end

NS_ASSUME_NONNULL_END
