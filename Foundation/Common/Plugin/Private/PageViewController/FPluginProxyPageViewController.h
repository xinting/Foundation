//
//  FPluginProxyPageViewController.h
//  FFoundation
//
//  Created by 三井 on 2020/10/13.
//

#import <UIKit/UIKit.h>
#import "FPluginHostProxy.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginProxyPageViewController : FPluginHostProxy <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@end

NS_ASSUME_NONNULL_END
