//
//  FPluginHostPageViewController.h
//  FFoundation
//
//  Created by 三井 on 2020/10/13.
//

#import "FPluginHost.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginHostPageViewController : FPluginHost

@property (nonatomic, strong) UIPageViewController *pageViewController;

@end

NS_ASSUME_NONNULL_END
