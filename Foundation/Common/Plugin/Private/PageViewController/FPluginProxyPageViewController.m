//
//  FPluginProxyPageViewController.m
//  FFoundation
//
//  Created by 三井 on 2020/10/13.
//

#import "FPluginProxyPageViewController.h"
#import "FPluginHeaders.h"

@interface FPluginProxyPageViewController ()

@property (nonatomic, weak) id<UIPageViewControllerDelegate> sourceDelegate;
@property (nonatomic, weak) id<UIPageViewControllerDataSource> sourceDataSource;

@end

@implementation FPluginProxyPageViewController

- (instancetype)initWithSource:(id)source {
    if (self = [super initWithSource:source]) {
        UIPageViewController *pageVC = source;
        self.sourceDataSource = pageVC.dataSource;
        self.sourceDelegate = pageVC.delegate;
        
        pageVC.dataSource = self;
        pageVC.delegate = self;
    }
    return self;
}

- (nullable UIViewController *)pageViewController:(nonnull UIPageViewController *)pageViewController viewControllerAfterViewController:(nonnull UIViewController *)viewController {
    NSUInteger index = [pageViewController.plg_datas indexOfObject:viewController];
    if (index == NSNotFound) {
        return pageViewController.plg_datas.firstObject;
    } else {
        if (index + 1 < pageViewController.plg_datas.count) {
            return pageViewController.plg_datas[index + 1];
        } else {
            return nil;
        }
    }
}

- (nullable UIViewController *)pageViewController:(nonnull UIPageViewController *)pageViewController viewControllerBeforeViewController:(nonnull UIViewController *)viewController {
    NSUInteger index = [pageViewController.plg_datas indexOfObject:viewController];
    if (index == NSNotFound) {
        return pageViewController.plg_datas.lastObject;
    } else {
        if (index - 1 >= 0 && index - 1 < pageViewController.plg_datas.count) {
            return pageViewController.plg_datas[index - 1];
        } else {
            return nil;
        }
    }
}

@end
