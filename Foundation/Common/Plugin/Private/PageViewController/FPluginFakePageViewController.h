//
//  FPluginFakePageViewController.h
//  FFoundation
//
//  Created by 三井 on 2020/10/13.
//

#import "FPluginFake.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginFakePageViewController : FPluginFake

@property (nonatomic, strong) id<UIPageViewControllerDataSource> dataSource;
@property (nonatomic, strong) id<UIPageViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
