//
//  FPluginProxyTabBarController.m
//  FFoundation
//
//  Created by 三井 on 2020/1/9.
//

#import "FPluginProxyTabBarController.h"
#import "FPluginFakeTabBarController.h"

@implementation FPluginProxyTabBarController

- (FPluginFake *)fakeForIdentifier:(NSString *)identifier {
    return [FPluginFakeTabBarController fakeFor:self.tabBarController];
}

- (UITabBarController *)tabBarController {
    return self.source;
}

@end
