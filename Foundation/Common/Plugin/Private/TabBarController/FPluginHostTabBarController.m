//
//  FPluginHostTabBarController.m
//  FFoundation
//
//  Created by 三井 on 2020/1/9.
//

#import "FPluginHostTabBarController.h"
#import "FPluginProxyTabBarController.h"

@implementation FPluginHostTabBarController

- (NSUInteger)loadPlugins:(FPluginArg *)arg {
    self.hostProxy = [[FPluginProxyTabBarController alloc] initWithSource:self.tabBarController];
    return [super loadPlugins:arg];
}

@end
