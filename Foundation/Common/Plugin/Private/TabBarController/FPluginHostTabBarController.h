//
//  FPluginHostTabBarController.h
//  FFoundation
//
//  Created by 三井 on 2020/1/9.
//

#import "FPluginHost.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginHostTabBarController : FPluginHost

@property (nonatomic, weak) UITabBarController *tabBarController;

@end

NS_ASSUME_NONNULL_END
