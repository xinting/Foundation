//
//  FPluginFake.h
//  Foundation
//
//  Created by 三井 on 2019/10/18.
//

#import <Foundation/Foundation.h>
#import "FPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@class FPluginWrappedData, FPluginHost;

/// 主要用于视图副本
@interface FPluginFake : NSProxy

@property (nonatomic, assign, getter=isFake, readonly) BOOL fake;

/// 与之相关的插件
@property (nonatomic, weak) id<FPlugin> plugin;

/// 真正的视图
@property (nonatomic, weak) id source;

+ (instancetype)fakeFor:(id)source;

/// 所在插件是否处理该数据——该数据由所在插件插入
/// @param data 数据
- (BOOL)canHandle:(FPluginWrappedData *)data;

- (FPluginHost *)pluginHost;
- (FPluginHost *)hostFor:(FPluginArg * _Nullable)arg;

@end

@interface NSObject (FPluginFake)

@property (nonatomic, assign, getter=isFake, readonly) BOOL fake;

@end

NS_ASSUME_NONNULL_END
