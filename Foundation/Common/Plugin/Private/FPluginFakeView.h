//
//  FPluginFakeView.h
//  FFoundation
//
//  Created by 三井 on 2019/10/30.
//

#import "FPluginFake.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginFakeView : FPluginFake

@property (nonatomic, assign, readonly, getter=isUpdating) BOOL update;

- (BOOL)plg_beginUpdate;
- (void)plg_endUpdate;

@end

NS_ASSUME_NONNULL_END
