//
//  FPluginHost.h
//  Foundation
//
//  Created by 新庭 on 2019/7/8.
//

#import <UIKit/UIKit.h>
#import "FPlugin.h"
#import "FPluginHostProxy.h"
#import "FPluginContext.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPluginHost : NSObject

@property (nonatomic, copy, readonly) NSString *hostIdentifier;

/**
 该主件相关的所有插件集合
 */
@property (nonatomic, copy, readonly) NSArray<id<FPlugin>> *plugins;

/// 插件被通知的顺序，输入插件id的数组。
@property (nonatomic, copy) NSArray<NSString *> *pluginPriority;
@property (nonatomic, strong, readonly) NSMutableArray *datas;    ///< 主件的数据集合，唯一

/// 实际与主视图交互的代理
/// 唯一跟真正的视图交互的委托和数据源
@property (nonatomic, strong) FPluginHostProxy *hostProxy;

/**
  * 主件相关的上下文，该上下文必须在load时传入，之后取出并更新。refresh时，插件系统，会将load时加入的值写回参数中。
 */
@property (nonatomic, strong, readonly) FPluginContext *context;

/**
 初始化一个主件实例

 @param hIdentifier 主件Id
 @return 实例
 */
- (instancetype)initWithHost:(NSString *)hIdentifier andPlugins:(NSArray<id<FPlugin>> *)plugins;
- (instancetype)initWithHost:(NSString *)hIdentifier;
- (void)reloadWithHost:(NSString *)hIdentifier;

/// 加载所有插件
/// @param arg 入参
- (NSUInteger)loadPlugins:(FPluginArg *)arg;
- (void)refreshPlugins:(FPluginArg *)arg;
- (void)unloadPlugins;

@end

NS_ASSUME_NONNULL_END
