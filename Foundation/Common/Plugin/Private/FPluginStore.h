//
//  FPluginStore.h
//  FFoundation
//
//  Created by 三井 on 2020/8/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class FPluginHost;
@interface FPluginStore : NSObject

@property (nonatomic, strong) NSPointerArray *hosts;

@end

NS_ASSUME_NONNULL_END
