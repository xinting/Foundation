//
//  FPluginStore.m
//  FFoundation
//
//  Created by 三井 on 2020/8/3.
//

#import "FPluginStore.h"

@implementation FPluginStore

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hosts = [NSPointerArray pointerArrayWithOptions:NSPointerFunctionsWeakMemory];
    }
    return self;
}

@end
