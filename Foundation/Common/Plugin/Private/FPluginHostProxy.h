//
//  FPluginHostProxy.h
//  Foundation
//
//  Created by 三井 on 2019/10/18.
//

#import <Foundation/Foundation.h>
#import "FPlugin.h"
#import "FPluginFake.h"
#import "FMacroDefine.h"

#define FPlugin_Proxy_Return_Arg __FPlugin_Proxy_Return_Arg_
/// 如果不属于插件，由主件处理
#define FPlugin_Proxy_Fake(_return_, _fake_, _src_, _fakeview_, _view_, ...) \
if ([_fake_ respondsToSelector:_cmd]) {\
    F_macro_msgSend(_return_, _fake_, _cmd, _fakeview_, __VA_ARGS__);\
    return;\
} else if ([_src_ respondsToSelector:_cmd]) {\
    F_macro_msgSend(_return_, _src_, _cmd, _view_, __VA_ARGS__);\
}\

/// 如果插件没有处理，转由插件处理
#define FPlugin_Proxy(_return_, _fake_, _src_, _fakeview_, _view_, ...) \
if ([_fake_ respondsToSelector:_cmd]) {\
    F_macro_msgSend(_return_, _fake_, _cmd, _fakeview_, __VA_ARGS__);\
    return;\
}\
if ([_src_ respondsToSelector:_cmd]) {\
    F_macro_msgSend(_return_, _src_, _cmd, _view_, __VA_ARGS__);\
}

/// 带返回值
#define FPlugin_Proxy_Return(_return_, _fake_, _src_, _fakeview_, _view_, ...) \
if ([_fake_ respondsToSelector:_cmd]) {\
    _return_ FPlugin_Proxy_Return_Arg = F_macro_msgSend(_return_, _fake_, _cmd, _fakeview_, __VA_ARGS__);\
    return FPlugin_Proxy_Return_Arg;\
}\
if ([_src_ respondsToSelector:_cmd]) {\
    _return_ FPlugin_Proxy_Return_Arg = F_macro_msgSend(_return_, _src_, _cmd, _view_, __VA_ARGS__);\
    return FPlugin_Proxy_Return_Arg;\
}

NS_ASSUME_NONNULL_BEGIN

@class FPluginWrappedData;

/// 该类实现集合视图的代理协议，并分发回调和事件
@interface FPluginHostProxy : NSObject

@property (nonatomic, weak, readonly) NSDictionary<NSString *, id> *pluginFakeMap;
@property (nonatomic, weak, readonly) id source;

- (instancetype)initWithSource:(id)source;

/// 主件代理填充参数
/// @param arg 将被传入到插件的参数
/// @param plugin 插件
- (FPluginFake *)prepareArg:(FPluginArg *)arg forPlugin:(id<FPlugin>)plugin;

/// 处理data的 fake 类实例
/// @param data 被处理的数据
- (FPluginFake *)fakeForData:(FPluginWrappedData *)data;

/// 根据ID生成需要的副本，需要子类重写
/// @param identifier id
- (FPluginFake *)fakeForIdentifier:(NSString *)identifier;

@end

NS_ASSUME_NONNULL_END
