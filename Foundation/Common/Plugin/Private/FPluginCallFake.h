//
//  FPluginCallFake.h
//  FFoundation
//
//  Created by 三井 on 2020/9/25.
//

#import "FPluginFake.h"

NS_ASSUME_NONNULL_BEGIN

/// 为任何对象的代理，如果对该对象的调用找不到方法，将返回nil
@interface FPluginCallFake : FPluginFake

@end

NS_ASSUME_NONNULL_END
