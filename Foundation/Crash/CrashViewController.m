//
//  CrashViewController.m
//  Foundation
//
//  Created by 三井 on 2021/6/30.
//  Copyright © 2021 iyinyue. All rights reserved.
//

#import "CrashViewController.h"

@interface CrashViewController ()

@end

@implementation CrashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"abort";
        item.action = ^{
            abort();
        };
    }];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"越界";
        item.action = ^{
            @[][1];
        };
    }];
    
    [self addItem:^(FTestItem * _Nonnull item) {
        item.title = @"Exception";
        item.action = ^{
            NSException *e = [NSException exceptionWithName:@"FCrash Exception" reason:@"Test Catch Crash" userInfo:@{}];
            @throw e;
        };
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
