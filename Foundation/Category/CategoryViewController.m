//
//  CategoryViewController.m
//  Foundation
//
//  Created by 三井 on 2020/10/10.
//  Copyright © 2020 iyinyue. All rights reserved.
//

#import "CategoryViewController.h"
#import <FFoundation/FXCategories.h>

@interface CategoryViewController ()

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *str = @"<div class=\"para\" label-module=\"para\"><div class=\"lemma-picture text-pic layout-right\" style=\"width:220px; float: right;\">\
    <a class=\"image-link\" nslog-type=\"9317\" href=\"/pic/12%E6%9C%8830%E6%97%A5/5231877/0/f31fbe096b63f6246b608569220dfcf81a4c510f050d?fr=lemma&amp;ct=single\" target=\"_blank\" title=\"梅艳芳\" style=\"width:220px;height:149.6px;\">\
    <img class=\"lazy-img\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRF9fX1AAAA0VQI3QAAAAxJREFUeNpiYAAIMAAAAgABT21Z4QAAAABJRU5ErkJggg==\" data-src=\"https://bkimg.cdn.bcebos.com/pic/f31fbe096b63f6246b608569220dfcf81a4c510f050d?x-bce-process=image/resize,m_lfit,w_220,limit_1\" alt=\"梅艳芳\" style=\"width:220px;height:149.6px;\">\
    </a>\
    <span class=\"description\">\
    梅艳芳\
    </span>\
    </div>2003年（癸未年）——中国香港著名歌手、演员<a target=\"_blank\" href=\"/item/%E6%A2%85%E8%89%B3%E8%8A%B3\">梅艳芳</a>逝世，终年四十岁。梅艳芳是二十世纪后半叶大中华地区的歌影巨星，<a target=\"_blank\" href=\"/item/%E9%A6%99%E6%B8%AF%E6%BC%94%E8%89%BA%E4%BA%BA%E5%8D%8F%E4%BC%9A\">香港演艺人协会</a>的主要创办人及首位女会长</div>";
    [str x_htmlRemoveClass:@"image-link"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
