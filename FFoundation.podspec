#
#  Be sure to run `pod spec lint Foundation.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "FFoundation"
  spec.version      = "1.3.3.1"
  spec.summary      = "Foundation Modules for xinting"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = <<-DESC

  经常使用的方法
                   DESC

  spec.homepage     = "https://gitee.com/xinting/Foundation"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  spec.license      = { :type => "MIT", :file => "LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  spec.author             = { "吴新庭" => "wu.xinting@hotmail.com" }
  # Or just: spec.author    = "吴新庭"
  # spec.authors            = { "吴新庭" => "wu.xinting@hotmail.com" }
  # spec.social_media_url   = "https://twitter.com/吴新庭"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  # spec.platform     = :ios
  spec.platform     = :ios, "9.0"

  #  When using multiple platforms
  # spec.ios.deployment_target = "5.0"
  # spec.osx.deployment_target = "10.7"
  # spec.watchos.deployment_target = "2.0"
  # spec.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  spec.source       = { :git => "https://gitee.com/xinting/Foundation.git", :tag => "#{spec.version}" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  spec.source_files  = "Foundation/Common/*.{h,m}"
  spec.exclude_files = "Foundation/Exclude"

  spec.public_header_files = "Foundation/Common/*.h"
  spec.prefix_header_file = "Foundation/Common/FoundationPrefix.pch"

  spec.subspec 'FBase' do |ss|
    ss.source_files = 'Foundation/Common/Base/**/*.{h,m}'
    ss.public_header_files = "Foundation/Common/Base/*.h"
  end
  
  spec.subspec 'FNetwork' do |ss|
    ss.source_files = 'Foundation/Common/Network/**/*.{h,m}'
    ss.public_header_files = 'Foundation/Common/Network/*.h'
    
    ss.dependency 'FFoundation/FBase'
    ss.dependency 'FFoundation/FCache'
    ss.dependency "AFNetworking"
  end
  
  spec.subspec 'FView' do |ss|
    ss.source_files = 'Foundation/Common/View/**/*.{h,m}'
    ss.prefix_header_file = 'Foundation/Common/View/Foundation-View-Prefix.pch'
    
    ss.resource_bundles = {
      'FView' => ['Foundation/Common/View/Resources/*.{xcassets}']
    }
    ss.dependency 'FFoundation/FBase'
    ss.dependency 'FFoundation/FCategories'
    ss.dependency 'FFoundation/FPlugin'
    ss.dependency 'Masonry'
    ss.dependency 'MSColorPicker'
    ss.dependency 'JKCategories'
  end
  
  spec.subspec 'FCategories' do |ss|
    ss.source_files = 'Foundation/Common/Categories/**/*.{h,m}'
    
    ss.dependency 'FFoundation/FBase'
  end
  
  spec.subspec 'FAlgorithm' do |ss|
    ss.source_files = 'Foundation/Common/Algorithm/**/*.{h,m,hpp,cpp,mm}'
  end
  
  spec.subspec 'FPlugin' do |ss|
    ss.source_files = ['Foundation/Common/Plugin/**/*.{h,m}']
    ss.public_header_files = ['Foundation/Common/Plugin/Host/*.h',
      'Foundation/Common/Plugin/Plugin/*.h',
      'Foundation/Common/Plugin/FPluginHeaders.h',
      'Foundation/Common/Plugin/**/*{.h}',
    ]
    
    ss.dependency 'FFoundation/FBase'
  end
  
  spec.subspec 'FWeb' do |ss|
    ss.source_files = 'Foundation/Common/Web/**/*.{h,m}'
    
    ss.resource_bundles = {
      'FWeb' => ['Foundation/Common/Web/Resources/*.{png,xcassets}']
    }
    ss.frameworks = 'WebKit'
    ss.dependency 'FFoundation/FBase'
    ss.dependency "Masonry"
  end
  
  spec.subspec 'FDownload' do |ss|
    ss.source_files = 'Foundation/Common/Download/**/*.{h,m}'
    ss.public_header_files = 'Foundation/Common/Download/*.{h}'
    
    ss.dependency 'YYModel'
    ss.dependency 'Reachability'
    ss.dependency 'SSZipArchive'
    ss.dependency "AFNetworking"
  end
  
  spec.subspec 'FPDF' do |ss|
    ss.source_files = 'Foundation/Common/PDF/**/*.{h,m}'
    
    ss.dependency 'FFoundation/FView'
    ss.frameworks = 'QuickLook'
  end
  
  spec.subspec 'FCache' do |ss|
    ss.source_files = 'Foundation/Common/Cache/**/*.{h,m}'
    
    ss.dependency 'YYModel'
    ss.dependency 'FFoundation/FBase'
  end
  
  spec.subspec 'FTest' do |ss|
    ss.source_files = 'Foundation/Common/Test/**/*.{h,m}'
    
    ss.dependency 'FFoundation/FCategories'
    ss.dependency 'Masonry'
  end
  
  spec.subspec 'FAudio' do |ss|
    ss.source_files = 'Foundation/Common/Audio/**/*.{h,m}'
  end
  
  spec.subspec 'FCrash' do |ss|
    ss.source_files = 'Foundation/Common/Crash/**/*.{h,m}'
    
    ss.dependency 'FFoundation/FTest'
  end
  
  spec.subspec 'FOffice' do |ss|
    ss.source_files = 'Foundation/Common/Office/**/*.{h,m}'
    ss.public_header_files = 'Foundation/Common/Office/**/*.h'
    ss.header_mappings_dir = 'Foundation/Common/Office/'
    ss.pod_target_xcconfig = { 'HEADER_SEARCH_PATHS' => '${PODS_ROOT}/Headers/Public/FFoundation/include/'}
    
    ss.vendored_libraries = 'Foundation/Common/Office/lib/libxlsxwriter-ios.a'
    ss.library = 'xlsxwriter-ios'
    
    ss.dependency 'FFoundation/FBase'
  end

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
  # spec.resources = "Foundation/Resources/**/*.{png,xcassets}"

  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"

  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  spec.xcconfig = { "HEADER_SEARCH_PATHS" => "${PODS_ROOT}/Headers/Public/FFoundation/include/" }
  spec.dependency 'JKCategories'
end
